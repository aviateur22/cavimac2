<?php

namespace Drupal\cavimac\Service;

class CotisationCalculationService {

  //approche de la modification
  /**
   * 1- Récupérer le statut final du membre + les TOP_RN/TOP_RP .....
   * 2- Déterminer les anciens codes CTP du membre modifié.
   * 3- Pour chaque "code CTP déterminé", récupération des informations suivantes:
   * - le taux de cotisation
   * - Le nombre total de personne
   * - Le montant total de l'assiette de cotisation
   * - Le montant total des cotisations
   * - La methode de calcul des cotisations
   * 4- Pour chaque "code CTP déterminé", calculer pour 1 personne:
   * - Le montant de l'assiette de cotisation
   * - Le montant des cotisations
   * 5- Recalculer pour chaque "code CTP déterminé":
   * - Le nombre total de personne - 1
   * - Le montant total de l'assiette de cotisation - Le montant de l'assiette pour 1 personne
   * - Le montant total des cotisations - Le montant des cotisations pour 1 personne
   * 6- Déterminer les nouveaux code CTP du membre modifié
   * 7- Pour chaque "nouveau code CTP", récupération des informations suivantes:
   * - le taux de cotisation
   * - Le nombre total de personne
   * - Le montant total de l'assiette de cotisation
   * - Le montant total des cotisations
   * - La methode de calcul des cotisations
   * 8- Pour chaque "nouveau code CTP", calculer pour 1 personne:
   * - Le montant de l'assiette de cotisation
   * - Le montant des cotisations
   * 9- Recalculer pour chaque "nouveau code CTP":
   * - Le nombre total de personne + 1
   * - Le montant total de l'assiette de cotisation + Le montant de l'assiette pour 1 personne
   * - Le montant total des cotisations + Le montant des cotisations pour 1 personne
   * 10- Mettre a jour la base de données
   */

   //information complete membre avant modification
   protected object $userBefore;

   //information complete membre aprés modification
   protected object $userAfter;

   //liste des code CTP rattaché à un membre avant modification
   protected array $userCtpBefore;

   //code collectivité
   protected string $communityCode;

   //liste des ctp rattachés a une colectivité
   protected array $communityCtp;

   //liste des codes CTP existant
   protected array $ctpCode;  

   /**
    * Extraction des codes CTP d'un collectivité pour un membre
    *@param array $communityCtp - liste des codes CTP pour une collectivité
    */
   function getMemberCTP($communityCtp) {

    //Code CTP pour le membre avant modification
    foreach($this-> communityCtp as $ctp) {
      if($this-> hasMemberCTP($ctp-> ctp_id, $this-> userBefore) === TRUE) {
        $this-> userCtpBefore [] = $ctp;
      }
    }

    //Code CTP pour le membre aprés modification
    foreach($this-> communityCtp as $ctp) {
      if($this-> hasMemberCTP($ctp-> ctp_id, $this-> userBefore) === TRUE) {
        $this-> userCtpAfter [] = $ctp;
      }
    }
   }

   function extractCommunityCTPInformation($communityCode) {

   }

   /**
    * Vérification si membre posséde un code CTP
    * @param integer $ctpCode - Code CTP a vérifier
    * @param object $user - information du membre 
    * @return boolean
    */
   function hasMemberCTP($ctpCode, $user) {

    if($ctpCode === 96 || $ctpCode === 185 &&
      (strtolower($user-> top_rn) === 'x' || strtolower($user-> top_rn) === 'i')) {

        return TRUE;

    } else if ($ctpCode === 95 || $ctpCode === 186 || $ctpCode === 187 &&
      (strtolower($user-> top_rp) === 'x' || strtolower($user-> top_rp) === 'i')) {

        return TRUE;
      
    } else if ($ctpCode === 97 || $ctpCode === 190 || $ctpCode === 191 || $ctpCode === 201 || $ctpCode === 203 && 
      (strtolower($user-> top_ro) === 'x' || strtolower($user-> top_ro) === 'i')) {

        return TRUE;

    } else if ($ctpCode >= 242 && $ctpCode <=255 && 
      (strtolower($user-> top_rco) === 'x' || strtolower($user-> top_rco) === 'i')) {

        return TRUE;

    } else if ($ctpCode === 260 && 
      (
        (strtolower($user-> top_csg1) === 'x' || strtolower($user-> top_csg1) === 'i')||
        (strtolower($user-> top_csg2) === 'x' || strtolower($user-> top_csg2) === 'i')|| 
        (strtolower($user-> top_csg3) === 'x' || strtolower($user-> top_csg3) === 'i')|| 
        (strtolower($user-> top_csg4) === 'x' || strtolower($user-> top_csg4) === 'i')
      )) {

        return TRUE;

    } else if ($ctpCode === 200 || $ctpCode ===202 &&
      (  
        (strtolower($user-> top_rn) === 'x' || strtolower($user-> top_rn) === 'i') ||
        (strtolower($user-> top_rp) === 'x' || strtolower($user-> top_rp) === 'i')
      )) {

        return TRUE;

    } else if ($ctpCode === 188 && 
      strtolower($user-> status) === 'ministre du culte') {

        return TRUE;

    } else if ($ctpCode === 189 && 
      (
        strtolower($user-> status) === 'ministre du culte' ||
        strtolower($user-> status) === 'ministre du culte')
      ) {

        return TRUE;

    } else if ($ctpCode === 204 || $ctpCode ===205) {

    } else {
      return false;
    }
  }

  /**
   * Calcule montant cotisation pour un code CTP
   * @param integer $ctpCode - Code CTP du membre
   * @return integer
   */
  function calculation($ctpCode) {
    if ($ctpCode === 95 || $ctpCode === 96 || $ctpCode === 97 || $ctpCode === 202 || $ctpCode === 203) {

    } else if( ($ctpCode >= 185 && $ctpCode <= 191) || ($ctpCode >= 242  && $ctpCode <= 255 ) || $ctpCode === 572) {

    } else if ($ctpCode === 200 || $ctpCode === 201 || $ctpCode === 204 || $ctpCode === 205) {

    } else if ($ctpCode === 260) {

    } else if($ctpCode === 261 || $ctpCode === 279) {

    }
  }

}
