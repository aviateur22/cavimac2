<?php
namespace Drupal\cavimac\Service;

class MemberStatusService {
  /**
   * Initialisation des statuts 
   * 1- Récupérer la liste des statuts disponibles
   * 2- Récuperer la liste des statuts calculés disponible
   * 3- Récupérer la liste des cultes
   * 4- recupérer le statut calculé
   * 5- réupérer le culte
   * 6- En fonction du culte et du statut -> déterminer le statut du membre
   * 7- renvoyer le statut ou null
   * 8- Si null -> erreur
   *  
   */
  /**
   * 1- Récupérer toutes les données actuelles du membre (status calculé, statut initial, cotisation, csg....)
   * 2- En fonction du statut du membre récupérer:
   * - Les nouveaux statuts disponibles
   * - les CSG liées aux nouveaux statuts
   * - "le statuts calculé" lié aux statuts
   * 3- Envoyer ces données vers le Front pour affichage et selections
   * 4- Renvoyer du Front-> Back le nouveau statut selectionné - ainsi que les CSG
   * 5- Si modification du statut: 
   * - Mettre a jours les les ETAT_RN, TOP_RN, ETAR_RP, TOP_RP ...., statut calculé et statut 
   * - Passer à la modification de la cotisation (CotisationCalculation.php)
   * 6- Sauvgarder les nouvelles données du membre
   */

  //Liste des cultes
  protected $cults = [
    [
      'index' => 1,
      'label'=>'DIOCESE'],
    [
      'index' => 2,
      'label'=>'CATHOLIQUE ROMAIN'],
    [
      'index' => 3,
      'label'=>'BOUDDHISTE'],
    [
      'index' => 4,
      'label'=>'ANGLICAN'],
    [
      'index' => 5,
      'label'=>'CATHOLIQUE PRECHALCEDONIEN'],
    [
      'index' => 6,
      'label'=>'MUSULMAN'],
    [
      'index' => 7,
      'label'=>'JUDAIQUE'],
    [
      'index' => 8,
      'label'=>"AUTRES CULTES D'ASIE"],
    [
      'index' => 9,
      'label'=>'ORTHODOXE'],
    [
      'index' => 10,
      'label'=>'PROTESTANT'],
    [
      'index' => 11,
      'label'=>'EVANGELIQUE'],
    [
      'index' => 12,
      'label'=>'ATHOLIQUE NON ROMAIN'],
    [
      'index' => 13,
      'label'=>"CULTES D'INSPIRATION CHRETIENNE"],
    [
      'index' => 14,
      'label'=>'TEMOINS DE JEHOVAH'],
    [
      'index' => 15,
      'label'=>'HINDOUISTE'],
    [
      'index' => 16,
      'label'=>'ADVENTISTE'
    ]
  ];

  //données en provenant de la doc
  protected $availableStatus = [
    [
      'index' => 1,
      'label'=>'COTISANT VOLONTAIRE'
    ],
    [
      'index' => 2,
      'label'=>'MINISTRE DU CULTE'
    ],
    [
      'index' => 3,
      'label'=>'MINISTRE DU CULTE - PENSIONNE CAVIMAC'
    ],
    [
      'index' => 4,
      'label'=>'MINISTRE DU CULTE - AA OU PENSIONNE AR'
    ],
    [
      'index' => 5,
      'label'=>'MEMBRE ACCUEILLI RN'
    ],
    [
      'index' => 6,
      'label'=>'MEMBRE ACCUEILLI RP'
    ],
    [
      'index' => 7,
      'label'=>'MEMBRE ACCUEILLI - AA OU PENSIONNE AR'
    ],
    [
      'index' => 8,
      'label'=>'SEMINARISTE - REGIME CAVIMAC'
    ],
    [
      'index' => 9,
      'label'=>'SEMINARISTE - AUTRE REGIME'
    ],    
    [
      'index' => 10,
      'label'=>'POSTULANT/NOVICE - REGIME CAVIMAC'
    ],
    [
      'index' => 11,
      'label'=>'POSTULANT/NOVICE - AUTRE REGIME'
    ],
    [
      'index' => 12,
      'label'=>'MINISTRE DU CULTE AVEC TRAITEMENT'
    ],
    [
      'index' => 13,
      'label'=>'MINISTRE DU CULTE SANS TRAITEMENT'
    ],
    [
      'index' => 14,
      'label'=>'MINISTRE DU CULTE AVEC TRT - AA OU P. AR'
    ],
    [
      'index' => 15,
      'label'=>'MINISTRE DU CULTE SANS TRT - AA OU P. AR'
    ],
    [
      'index' => 16,
      'label'=>'MEMBRE RELIGIEUX RN'
    ],
    [
      'index' => 17,
      'label'=>'MEMBRE RELIGIEUX RP'
    ],
    [
      'index' => 18,
      'label'=>'MEMBRE RELIGIEUX - AA OU PENSIONNE AR'
    ],
    [
      'index' => 19,
      'label'=>'MINISTRE DU CULTE SANS ACTIVITE'
    ],
    [
      'index' => 20,
      'label'=>'MINISTRE DU CULTE - AA OU PENSIONNE AR'
    ],
    [
      'index' => 21,
      'label'=>'RADIE'
    ]
  ];

  //données provenant de la doc
  protected $calculatedStatus = [
    [
      'index'=> 1,
      'label'=> 'COTISANT VOLONTAIRE'
    ],
    [
      'index'=> 2,
      'label'=> 'MINISTRE DU CULTE'
    ],
    [
      'index'=> 3,
      'label'=> 'MINISTRE DU CULTE - PENSIONNE CAVIMAC'
    ],
    [
      'index'=> 4,
      'label'=> 'MINISTRE DU CULTE - AA OU PENSIONNE AR'
    ],
    [
      'index'=> 5,
      'label'=> 'MEMBRE ACCUEILLI RN'
    ],
    [
      'index'=> 6,
      'label'=> 'MEMBRE ACCUEILLI RP'
    ],
    [
      'index'=> 7,
      'label'=> 'MEMBRE ACCUEILLI - AA OU PENSIONNE AR'
    ],
    [
      'index'=> 8,
      'label'=> 'SEMINARISTE - REGIME CAVIMAC'
    ],
    [
      'index'=> 9,
      'label'=> 'SEMINARISTE - AUTRE REGIME'
    ],
    [
      'index'=> 10,
      'label'=> 'MEMBRE RELIGIEUX RN'
    ],
    [
      'index'=> 11,
      'label'=> 'MEMBRE RELIGIEUX RP'
    ],
    [
      'index'=> 12,
      'label'=> 'MEMBRE RELIGIEUX - AA OU PENSIONNE AR'
    ],
    [
      'index'=> 13,
      'label'=> 'POSTULANT/NOVICE - REGIME CAVIMAC'
    ],
    [
      'index'=> 14,
      'label'=> 'POSTULANT/NOVICE - AUTRE REGIME'
    ],
    [
      'index'=> 15,
      'label'=> 'MINISTRE DU CULTE AVEC TRAITEMENT'
    ],
    [
      'index'=> 16,
      'label'=> 'MINISTRE DU CULTE SANS TRAITEMENT'
    ],
    [
      'index'=> 17,
      'label'=> 'MINISTRE DU CULTE AVEC TRT - AA OU P. AR'
    ],
    [
      'index'=> 18,
      'label'=> 'MINISTRE DU CULTE SANS TRT - AA OU P. AR'
    ],
    [
      'index'=> 19,
      'label'=> 'MINISTRE DU CULTE SANS ACTIVITE'
    ],
    [
      'index'=> 20,
      'label'=> 'RADIE'
    ]
  ];

  //données converties sans accent et en minuscule
  protected $statusToLower;
  protected $statusCalculatedToLower;
  protected $cultToLower;

  //pour Les liste extraite de SQL
  protected  $statusList;
  protected  $calculatedStatusList;



  /**
   * Initialisation des services
   * @param \Drupal\cavimac\Service\ImportDataQueryService $importDataQueryService
   * @param \Drupal\cavimac\Service\TableService $tableService
   * @param \Drupal\cavimac\Service\UtilityService $utilityService
   */
  function __construct($importDataQueryService, $tableService, $utilityService) {  
    $this-> importDataQueryService = $importDataQueryService ;   
    $this-> tableService = $tableService ; 
    $this-> utilityService = $utilityService;

    //récupération des statuts initiaux
    $this-> statusList = $this-> importDataQueryService-> selectAll($this-> tableService-> specStatusTable());
    
    $this-> statusToLower = $this-> arrayToLowerCase($this->availableStatus);

    //liste des cultes en miniscule
    $this-> cultToLower = $this-> arrayToLowerCase($this->cults);

    //liste des statuts calculé en minuscule
    $this-> statusCalculatedToLower = $this-> arrayToLowerCase($this->calculatedStatus);
  }

  /**
   * Contenu tableau en lowerCase
   * @param array $array - Tableau a modifier
   * @return array
   */
  function arrayToLowerCase($array) {
    
    for($i = 0; $i < count($array); $i++) {
      
      $array[$i]['label'] = strtolower($array[$i]['label']);
      //$array[$i]['label'] = $this->unaccent(strtolower($array[$i]['label']));
    }
    //dump( $array[$i]['label']);   
    return $array;
  }

  /**
   * Supprime les accents d'une text
   * @param string $str
   * @return string
   */
  function unaccent($str)  {
    $transliteration = array(
      'Ĳ' => 'I', 'Ö' => 'O','Œ' => 'O','Ü' => 'U','ä' => 'a','æ' => 'a',
      'ĳ' => 'i','ö' => 'o','œ' => 'o','ü' => 'u','ß' => 's','ſ' => 's',
      'À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A',
      'Æ' => 'A','Ā' => 'A','Ą' => 'A','Ă' => 'A','Ç' => 'C','Ć' => 'C',
      'Č' => 'C','Ĉ' => 'C','Ċ' => 'C','Ď' => 'D','Đ' => 'D','È' => 'E',
      'É' => 'E','Ê' => 'E','Ë' => 'E','Ē' => 'E','Ę' => 'E','Ě' => 'E',
      'Ĕ' => 'E','Ė' => 'E','Ĝ' => 'G','Ğ' => 'G','Ġ' => 'G','Ģ' => 'G',
      'Ĥ' => 'H','Ħ' => 'H','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I',
      'Ī' => 'I','Ĩ' => 'I','Ĭ' => 'I','Į' => 'I','İ' => 'I','Ĵ' => 'J',
      'Ķ' => 'K','Ľ' => 'K','Ĺ' => 'K','Ļ' => 'K','Ŀ' => 'K','Ł' => 'L',
      'Ñ' => 'N','Ń' => 'N','Ň' => 'N','Ņ' => 'N','Ŋ' => 'N','Ò' => 'O',
      'Ó' => 'O','Ô' => 'O','Õ' => 'O','Ø' => 'O','Ō' => 'O','Ő' => 'O',
      'Ŏ' => 'O','Ŕ' => 'R','Ř' => 'R','Ŗ' => 'R','Ś' => 'S','Ş' => 'S',
      'Ŝ' => 'S','Ș' => 'S','Š' => 'S','Ť' => 'T','Ţ' => 'T','Ŧ' => 'T',
      'Ț' => 'T','Ù' => 'U','Ú' => 'U','Û' => 'U','Ū' => 'U','Ů' => 'U',
      'Ű' => 'U','Ŭ' => 'U','Ũ' => 'U','Ų' => 'U','Ŵ' => 'W','Ŷ' => 'Y',
      'Ÿ' => 'Y','Ý' => 'Y','Ź' => 'Z','Ż' => 'Z','Ž' => 'Z','à' => 'a',
      'á' => 'a','â' => 'a','ã' => 'a','ā' => 'a','ą' => 'a','ă' => 'a',
      'å' => 'a','ç' => 'c','ć' => 'c','č' => 'c','ĉ' => 'c','ċ' => 'c',
      'ď' => 'd','đ' => 'd','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e',
      'ē' => 'e','ę' => 'e','ě' => 'e','ĕ' => 'e','ė' => 'e','ƒ' => 'f',
      'ĝ' => 'g','ğ' => 'g','ġ' => 'g','ģ' => 'g','ĥ' => 'h','ħ' => 'h',
      'ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ī' => 'i','ĩ' => 'i',
      'ĭ' => 'i','į' => 'i','ı' => 'i','ĵ' => 'j','ķ' => 'k','ĸ' => 'k',
      'ł' => 'l','ľ' => 'l','ĺ' => 'l','ļ' => 'l','ŀ' => 'l','ñ' => 'n',
      'ń' => 'n','ň' => 'n','ņ' => 'n','ŉ' => 'n','ŋ' => 'n','ò' => 'o',
      'ó' => 'o','ô' => 'o','õ' => 'o','ø' => 'o','ō' => 'o','ő' => 'o',
      'ŏ' => 'o','ŕ' => 'r','ř' => 'r','ŗ' => 'r','ś' => 's','š' => 's',
      'ť' => 't','ù' => 'u','ú' => 'u','û' => 'u','ū' => 'u','ů' => 'u',
      'ű' => 'u','ŭ' => 'u','ũ' => 'u','ų' => 'u','ŵ' => 'w','ÿ' => 'y',
      'ý' => 'y','ŷ' => 'y','ż' => 'z','ź' => 'z','ž' => 'z','Α' => 'A',
      'Ά' => 'A','Ἀ' => 'A','Ἁ' => 'A','Ἂ' => 'A','Ἃ' => 'A','Ἄ' => 'A',
      'Ἅ' => 'A','Ἆ' => 'A','Ἇ' => 'A','ᾈ' => 'A','ᾉ' => 'A','ᾊ' => 'A',
      'ᾋ' => 'A','ᾌ' => 'A','ᾍ' => 'A','ᾎ' => 'A','ᾏ' => 'A','Ᾰ' => 'A',
      'Ᾱ' => 'A','Ὰ' => 'A','ᾼ' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D',
      'Ε' => 'E','Έ' => 'E','Ἐ' => 'E','Ἑ' => 'E','Ἒ' => 'E','Ἓ' => 'E',
      'Ἔ' => 'E','Ἕ' => 'E','Ὲ' => 'E','Ζ' => 'Z','Η' => 'I','Ή' => 'I',
      'Ἠ' => 'I','Ἡ' => 'I','Ἢ' => 'I','Ἣ' => 'I','Ἤ' => 'I','Ἥ' => 'I',
      'Ἦ' => 'I','Ἧ' => 'I','ᾘ' => 'I','ᾙ' => 'I','ᾚ' => 'I','ᾛ' => 'I',
      'ᾜ' => 'I','ᾝ' => 'I','ᾞ' => 'I','ᾟ' => 'I','Ὴ' => 'I','ῌ' => 'I',
      'Θ' => 'T','Ι' => 'I','Ί' => 'I','Ϊ' => 'I','Ἰ' => 'I','Ἱ' => 'I',
      'Ἲ' => 'I','Ἳ' => 'I','Ἴ' => 'I','Ἵ' => 'I','Ἶ' => 'I','Ἷ' => 'I',
      'Ῐ' => 'I','Ῑ' => 'I','Ὶ' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M',
      'Ν' => 'N','Ξ' => 'K','Ο' => 'O','Ό' => 'O','Ὀ' => 'O','Ὁ' => 'O',
      'Ὂ' => 'O','Ὃ' => 'O','Ὄ' => 'O','Ὅ' => 'O','Ὸ' => 'O','Π' => 'P',
      'Ρ' => 'R','Ῥ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Ύ' => 'Y',
      'Ϋ' => 'Y','Ὑ' => 'Y','Ὓ' => 'Y','Ὕ' => 'Y','Ὗ' => 'Y','Ῠ' => 'Y',
      'Ῡ' => 'Y','Ὺ' => 'Y','Φ' => 'F','Χ' => 'X','Ψ' => 'P','Ω' => 'O',
      'Ώ' => 'O','Ὠ' => 'O','Ὡ' => 'O','Ὢ' => 'O','Ὣ' => 'O','Ὤ' => 'O',
      'Ὥ' => 'O','Ὦ' => 'O','Ὧ' => 'O','ᾨ' => 'O','ᾩ' => 'O','ᾪ' => 'O',
      'ᾫ' => 'O','ᾬ' => 'O','ᾭ' => 'O','ᾮ' => 'O','ᾯ' => 'O','Ὼ' => 'O',
      'ῼ' => 'O','α' => 'a','ά' => 'a','ἀ' => 'a','ἁ' => 'a','ἂ' => 'a',
      'ἃ' => 'a','ἄ' => 'a','ἅ' => 'a','ἆ' => 'a','ἇ' => 'a','ᾀ' => 'a',
      'ᾁ' => 'a','ᾂ' => 'a','ᾃ' => 'a','ᾄ' => 'a','ᾅ' => 'a','ᾆ' => 'a',
      'ᾇ' => 'a','ὰ' => 'a','ᾰ' => 'a','ᾱ' => 'a','ᾲ' => 'a','ᾳ' => 'a',
      'ᾴ' => 'a','ᾶ' => 'a','ᾷ' => 'a','β' => 'b','γ' => 'g','δ' => 'd',
      'ε' => 'e','έ' => 'e','ἐ' => 'e','ἑ' => 'e','ἒ' => 'e','ἓ' => 'e',
      'ἔ' => 'e','ἕ' => 'e','ὲ' => 'e','ζ' => 'z','η' => 'i','ή' => 'i',
      'ἠ' => 'i','ἡ' => 'i','ἢ' => 'i','ἣ' => 'i','ἤ' => 'i','ἥ' => 'i',
      'ἦ' => 'i','ἧ' => 'i','ᾐ' => 'i','ᾑ' => 'i','ᾒ' => 'i','ᾓ' => 'i',
      'ᾔ' => 'i','ᾕ' => 'i','ᾖ' => 'i','ᾗ' => 'i','ὴ' => 'i','ῂ' => 'i',
      'ῃ' => 'i','ῄ' => 'i','ῆ' => 'i','ῇ' => 'i','θ' => 't','ι' => 'i',
      'ί' => 'i','ϊ' => 'i','ΐ' => 'i','ἰ' => 'i','ἱ' => 'i','ἲ' => 'i',
      'ἳ' => 'i','ἴ' => 'i','ἵ' => 'i','ἶ' => 'i','ἷ' => 'i','ὶ' => 'i',
      'ῐ' => 'i','ῑ' => 'i','ῒ' => 'i','ῖ' => 'i','ῗ' => 'i','κ' => 'k',
      'λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'k','ο' => 'o','ό' => 'o',
      'ὀ' => 'o','ὁ' => 'o','ὂ' => 'o','ὃ' => 'o','ὄ' => 'o','ὅ' => 'o',
      'ὸ' => 'o','π' => 'p','ρ' => 'r','ῤ' => 'r','ῥ' => 'r','σ' => 's',
      'ς' => 's','τ' => 't','υ' => 'y','ύ' => 'y','ϋ' => 'y','ΰ' => 'y',
      'ὐ' => 'y','ὑ' => 'y','ὒ' => 'y','ὓ' => 'y','ὔ' => 'y','ὕ' => 'y',
      'ὖ' => 'y','ὗ' => 'y','ὺ' => 'y','ῠ' => 'y','ῡ' => 'y','ῢ' => 'y',
      'ῦ' => 'y','ῧ' => 'y','φ' => 'f','χ' => 'x','ψ' => 'p','ω' => 'o',
      'ώ' => 'o','ὠ' => 'o','ὡ' => 'o','ὢ' => 'o','ὣ' => 'o','ὤ' => 'o',
      'ὥ' => 'o','ὦ' => 'o','ὧ' => 'o','ᾠ' => 'o','ᾡ' => 'o','ᾢ' => 'o',
      'ᾣ' => 'o','ᾤ' => 'o','ᾥ' => 'o','ᾦ' => 'o','ᾧ' => 'o','ὼ' => 'o',
      'ῲ' => 'o','ῳ' => 'o','ῴ' => 'o','ῶ' => 'o','ῷ' => 'o','А' => 'A',
      'Б' => 'B','В' => 'V','Г' => 'G','Д' => 'D','Е' => 'E','Ё' => 'E',
      'Ж' => 'Z','З' => 'Z','И' => 'I','Й' => 'I','К' => 'K','Л' => 'L',
      'М' => 'M','Н' => 'N','О' => 'O','П' => 'P','Р' => 'R','С' => 'S',
      'Т' => 'T','У' => 'U','Ф' => 'F','Х' => 'K','Ц' => 'T','Ч' => 'C',
      'Ш' => 'S','Щ' => 'S','Ы' => 'Y','Э' => 'E','Ю' => 'Y','Я' => 'Y',
      'а' => 'A','б' => 'B','в' => 'V','г' => 'G','д' => 'D','е' => 'E',
      'ё' => 'E','ж' => 'Z','з' => 'Z','и' => 'I','й' => 'I','к' => 'K',
      'л' => 'L','м' => 'M','н' => 'N','о' => 'O','п' => 'P','р' => 'R',
      'с' => 'S','т' => 'T','у' => 'U','ф' => 'F','х' => 'K','ц' => 'T',
      'ч' => 'C','ш' => 'S','щ' => 'S','ы' => 'Y','э' => 'E','ю' => 'Y',
      'я' => 'Y','ð' => 'd','Ð' => 'D','þ' => 't','Þ' => 'T','ა' => 'a',
      'ბ' => 'b','გ' => 'g','დ' => 'd','ე' => 'e','ვ' => 'v','ზ' => 'z',
      'თ' => 't','ი' => 'i','კ' => 'k','ლ' => 'l','მ' => 'm','ნ' => 'n',
      'ო' => 'o','პ' => 'p','ჟ' => 'z','რ' => 'r','ს' => 's','ტ' => 't',
      'უ' => 'u','ფ' => 'p','ქ' => 'k','ღ' => 'g','ყ' => 'q','შ' => 's',
      'ჩ' => 'c','ც' => 't','ძ' => 'd','წ' => 't','ჭ' => 'c','ხ' => 'k',
      'ჯ' => 'j','ჰ' => 'h' 
      );
    $str = str_replace( array_keys( $transliteration ),array_values( $transliteration ), $str);
    return $str;
  }

  #region Affichage des status
  /**
   * Statut disponible pour un membre
   * @param string $cult - Culte actuel du membre
   * @param string $status - Statut actuel du membre
   */
  public function statusAvailable($cult, $status) { 

    //index du culte du memebre
    $cultIndex = array_search(strtolower($cult), array_column($this-> cultToLower, 'label'));

    if(!is_null($status)) {
      $statusIndex = array_search(strtolower($status), array_column($this-> statusToLower, 'label'));  
    }

    //Si culte pas trouvé
    if(!is_int($cultIndex)){
      return null;
    }

    //Récupération des status disponible
    $memberStatusAvailable = $this->getAvailStatus($this-> cults[$cultIndex], is_null($status) ? null : $this-> availableStatus[$statusIndex]);
    return $memberStatusAvailable;
    // dump($memberStatusAvailable);

    // return 
    //   [
    //     [
    //       "status" =>'Assuré a titre Volontaire',
    //       'csg' => '1,2,3,4,5'
    //     ],        
    //     [
    //       "status" =>'Ministre du culte',
    //       'csg' => '1,2'
    //     ],
    //     [
    //       "status" =>'Séminiraiste - Cavimac',
    //       'csg' => ''
    //     ],
    //   ];
  }

  /**
   * Récupération des status disponible suivant le culte du membre
   * @param object $cult - culte du membre
   * @param object|null $status -  statut du membre
   */
  public function getAvailStatus($cult, $status) {

    // culte dioces
    if($cult['index'] === 1) {
      return $this-> diocesStatus($status);      
    }

    if($cult['index'] === 1) {

    }

    if($cult['index'] === 1) {

    }

    if($cult['index'] === 1) {

    }

    if($cult['index'] === 1) {

    }

    if($cult['index'] === 1) {

    }

  }

  #region Dioces
    /**
     * traitement des cultes Dioces
     * @param object $status - status du membre
     */
    public function diocesStatus($status) {
      //Liste des statuts validés      
      $diocesStatusAvailable = $this-> diocesStatusAvailable();

      if(is_null($status)) {
        $statusAvail =[1, 2, 3, 4, 5, 6 , 7, 8, 9];        
        return $this-> setResponse($statusAvail, $diocesStatusAvailable);      

      } else if($status['index'] === 5 || $status['index'] === 6 || $status['index'] === 7) {
        $statusAvail =[1, 2, 3, 4, 5, 6 , 7, 8, 9];

        return  $this-> setResponse($statusAvail, $diocesStatusAvailable);

      } else if($status['index'] === 1 || $status['index'] === 2) {
        $statusAvail =[5, 6, 7, 24];
        return  $this-> setResponse($statusAvail, $diocesStatusAvailable);

      } else if($status['index'] === 3 || $status['index'] === 4) {
        $statusAvail =[3, 4, 24];
        return  $this-> setResponse($statusAvail, $diocesStatusAvailable);

      } else if($status['index'] === 8 || $status['index'] === 9) {
        $statusAvail =[1, 2, 8, 9, 24];
        return  $this-> setResponse($statusAvail, $diocesStatusAvailable);
      }
    }

    /**
     * Renvoie la liste des statuts Dioces qui sont disponible
     * @return array 
     */
    function diocesStatusAvailable() {
      return [
        //statut calculé "cotisant volontaire"
        1 => [
          'index' => 1,
          //csg pour le statut
          'csg' => '-',
          //rn - rp - ro - rco - rv
          'cotisation' => 'false,false,false,false,true',
          //statut calculé
          'finalStatus' => 1
        ],
        2 => [
          'index' => 2,
          'csg' => '1,2,3',
          'cotisation' => 'true,false,true,true,false',
          'finalStatus' => 1
        ],
        3 => [
          'index' => 3,
          'csg' => '4,5',
          'cotisation' => 'false,false,false,false,false',
          'finalStatus' => 1
        ],
        4 => [
          'index' => 4,
          'csg' => '1,2,3',
          'cotisation' => 'false,false,true,false,false',
          'finalStatus' => 1
        ],
        5 => [
          'index' => 5,
          'csg' => '-',
          'cotisation' => 'true,false,true,false,false',
          'finalStatus' => 1
        ],
        6 => [
          'index' => 6,
          'csg' => '-',
          'cotisation' => 'false,true,true,false,false',
          'finalStatus' => 1
        ],
        7 => [
          'index' => 7,
          'csg' => '-',
          'cotisation' => 'false,false,true,false,false',
          'finalStatus' => 1
        ],
        8 => [
          'index' => 8,
          'csg' => '-',
          'cotisation' => 'true,false,true,false,false',
          'finalStatus' => 1
        ],
        9 => [
          'index' => 9,
          'csg' => '-',
          'cotisation' => 'false,false,true,false,false',
          'finalStatus' => 1
        ],
        24 => [
          'index' => 24,
          'csg' => '-',
          'cotisation' => 'false,false,false,false,false',
          'finalStatus' => 1
        ]
      ];
    }

  #endregion

  /**
   * Formate les statuts
   * @param array $statusAvail - liste des statuts disponable suivant le statut
   * @param array $statusValidated - listes des statuts pour un culte
   * @return array
   */
  function setResponse($statusAvail, $statusValidated) {
    $dataArray = [];
    foreach($statusAvail as $status) {     
      
      $index = $statusValidated[$status]['index'];
      $statusCsg = $statusValidated[$status]['csg'];
      $statusCotisation = $statusValidated[$status]['cotisation'];
      
      $statusFind = array_search($index, array_column($this-> statusToLower, 'index'));
      $statusText = $this-> statusToLower[$statusFind]['label'];
      $statusIndex = $this-> statusToLower[$statusFind]['index'];
      
      $dataArray [] = [
        "status" => $statusText,
        "statusIndex" => $statusIndex,
        "csg" => $statusCsg,
        "cotisation" => $statusCotisation
      ];
    }    
    return $dataArray;
  }
  #endregion
}

