<?php
namespace Drupal\cavimac\Service;

/**
 * Initialisation des statuts des membres
 */
class MemberStatusInitializeService {
    /**
   * Initialisation des statuts 
   * 1- Récupérer la liste des statuts disponibles
   * 2- Récuperer la liste des statuts calculés disponible
   * 3- Récupérer la liste des cultes
   * 4- Récupérer les cotisations d'un membre (RN-RP-RO-RCO......)
   * 6- En fonction du culte et du statut -> déterminer le statut initial  du membre
   * 7- renvoyer le statut ou null
   * 8- Si null -> Stocke le membre dans un tableau
   * 9- Si count(Tableau erreur)>0 alors erreur
   */

  //Statut Liste
  protected $statusList;

   /**
   * Initialisation des services
   * @param \Drupal\cavimac\Service\ImportDataQueryService $importDataQueryService
   * @param \Drupal\cavimac\Service\TableService $tableService  
   * @param \Drupal\cavimac\Service\utilityStatusService $utilityStatusService
   */
    function __construct($importDataQueryService, $tableService, $utilityStatusService) {
        $this->importDataQueryService = $importDataQueryService ;   
        $this->tableService = $tableService; 
        $this->utilityStatusService = $utilityStatusService;

        //récupération des statuts initiaux
        $this->statusList = $this->importDataQueryService->selectAll($this->tableService-> specStatusTable());
    }  

  /**
   * Validation du Statut et TOP 
   *
   * @param array $dataArray - Données des membres
   * @return void
   */ 
  public function statusInitialization($dataArray) {
    /**
     * Position des données 
     * $dataArray[$i][10] status calculé
     * $dataArray[$i][31] culte
     * $dataArray[$i][11] rn
     * $dataArray[$i][13] rp
     * $dataArray[$i][15] ro
     * $dataArray[$i][17] rco
     * $dataArray[$i][19] csg1
     * $dataArray[$i][21] csg2
     * $dataArray[$i][23] csg3
     * $dataArray[$i][25] csg4
     * $dataArray[$i][27] csg5
     * $dataArray[$i][29] rv
     */

      //Stock les membre aynt des erreurs d'initialisation
      $arrayMemberError = [];

      for($i = 0; $i < count($dataArray); $i++ ) {
        //statut calculé
        $status = $dataArray[$i][10];

        //culte
        $cult = $dataArray[$i][31];

        //Récupére les cotisations d'un membre 
        $cotisationArray = [
          $dataArray[$i][11],
          $dataArray[$i][13],
          $dataArray[$i][15],
          $dataArray[$i][17],
          $dataArray[$i][19],
          $dataArray[$i][21],
          $dataArray[$i][23],
          $dataArray[$i][25],
          $dataArray[$i][27],
          $dataArray[$i][29]
          ];

          //Convertie les cotisation des membres en format string
          $cotisationString = $this->cotisationToString($cotisationArray);
          
          // détermination du statut initial
          $statusInitial = $this->initialstatus((int) $cult, $status, $cotisationString);  
          
          //dump($statusInitial);
          
          if(empty($statusInitial) || is_null($statusInitial)) {
            //Ajout du membre en erreur
            $arrayMemberError[] = $dataArray[$i];
          } else {
            // Ajout du status initial dans les données    
            $dataArray[$i][] = $statusInitial;
          }
      }

      // Si erreur d'initialisation des membres
      if(count($arrayMemberError) > 0) {
        return $this-> reportMemberErrorInitializtion($arrayMemberError);
      }
  }

  /**
   * Renvoie les cotisations d'un mmebre en format string du type
   * 'false,false,false,false,false,false,true,false,true,true' 
   * @param array arrayData - données du membre
   */
    function cotisationToString($arrayData) {
        $cotisationString = '';
        
        foreach($arrayData as $data) {
          if( strtolower($data) === 'x' || strtolower($data) === 'i') {
              $cotisationString .= 'true';

          } else {
              $cotisationString .= 'false';
          }
          $cotisationString .= ','; 
        }

        return substr_replace($cotisationString,"",-1);
    }

  /**
   * Renvoie le statut initial d'un membre en fct de son culte et statut calculé
   * @param string $cultIndex - id du culte
   * @param string $calculatedStatusIndex - id du statut calculé
   * @param string $cotisationString - cotisation du membre
   * @return string|null - id statut initial ou null si pas trouvé
   */
  function initialstatus($cultIndex, $calculatedStatusIndex, $cotisationString) {

    switch ($cultIndex) {
      //Culte Diocese
      case $this->utilityStatusService::CULT['DIOCESE']:        
        return $this->findCotisation($this->utilityStatusService::DIOCES_COTISATION, (int) $calculatedStatusIndex, $cotisationString);
        break;

      //Culte Romain
      case $this->utilityStatusService::CULT['CATHOLIQUE_ROMAIN']:
        return $this->findCotisation($this->utilityStatusService::CATHO_ROMAIN_COTISATION, (int) $calculatedStatusIndex, $cotisationString);
        break;

      //Culte Anglican - catholique Préchalcédonien - Musulman - Judaïque - Autres culte d'Asie
      case in_array($cultIndex,
       [
        $this->utilityStatusService::CULT['ANGLICAN'],
        $this->utilityStatusService::CULT['CATHOLIQUE_PRECHALCEDONIEN'], 
        $this->utilityStatusService::CULT['MUSULMAN'], 
        $this->utilityStatusService::CULT['JUDAIQUE'],
        $this->utilityStatusService::CULT['AUTRES_CULTES_D\'ASIE']
        ]):
        return $this->findCotisation($this->utilityStatusService::ANGLICAN_COTISATION, (int) $calculatedStatusIndex, $cotisationString);
        break;

      //Bouddhiste
      case $this->utilityStatusService::CULT['BOUDDHISTE' ]:
        return $this->findCotisation($this->utilityStatusService::BOUDDHISTE_COTISATION, (int) $calculatedStatusIndex, $cotisationString);
        break;

      //Orthodoxe
      case $this->utilityStatusService::CULT['ORTHODOXE']:
        return $this->findCotisation($this->utilityStatusService::ORTHODOX_COTISATION, (int) $calculatedStatusIndex, $cotisationString);
        break;

      //Protestant - evangelique
      case $this->utilityStatusService::CULT['PROTESTANT_EVANGELIQUE']:
        return $this->findCotisation($this->utilityStatusService::PROT_EVAN_COTISATION, (int) $calculatedStatusIndex, $cotisationString);
        break;

      //Catho non romain - Inspiration chrétienne - Témoin de jehovah - Hindouiste
      case in_array($cultIndex, 
        [
          $this->utilityStatusService::CULT['CATHOLIQUE_NON_ROMAIN'],
          $this->utilityStatusService::CULT['CULTES_D\'INSPIRATION_CHRETIENNE'],
          $this->utilityStatusService::CULT['TEMOINS_DE_JEHOVAH'],
          $this->utilityStatusService::CULT['HINDOUISTE'],
        ]):
        return $this->findCotisation($this->utilityStatusService::CATHO_NON_ROMAIN_COTISATION, (int) $calculatedStatusIndex, $cotisationString);
        break;

      //Adventiste
      case $this->utilityStatusService::CULT['ADVENTISTE' ]:
        return $this->findCotisation($this->utilityStatusService::ADVENTISTE_COTISATION, (int) $calculatedStatusIndex, $cotisationString);
        break;

      default: 
        return null;      
    }
  }

  /**
   * Vérifie l'existence d'une cotisation en fonction d'un culte et d'un statut
   *
   * @param array $statusCotisationArray - Liste de cotisatations disponible pour un culte et ces statuts
   * @param string $calculatedStatusIndex - status du membre
   * @param string $cotisation - cotisation du membre
   * @return int|null - Renvoie l'index du statut du membre si trouvé ou null'
   */
  function findCotisation($statusCotisationArray, $calculatedStatusIndex, $cotisation) {
    $cotisationAvail = $this->utilityStatusService->findCotisation($statusCotisationArray, $calculatedStatusIndex);
    
    if(!is_int(array_search($cotisation, $cotisationAvail))) {
      return null;
    }
    return ($calculatedStatusIndex);
  }

  /**
   * Envoie d'un message indiquant les membres en erreur d'initialisation
   *
   * @param array $errorArray - liste des membres ayant une erreur d'initialisation
   * @return void
   */
  function reportMemberErrorInitializtion($errorArray) {    
    dump('Erreur d\'initialisation pour les membres suivants: ');
    dump($errorArray);
    throw new \Exception('Initialization member error for: ' . $this->stringFromArray($errorArray), 400);
  }

  /**
   * Convertion d'un Array en string
   *
   * @param array $arrayData - Array a convertir en string
   * @return string - Array converti en string
   */
  function stringFromArray($arrayData) {
    return implode(',', $arrayData);
  }
}