<?php
namespace Drupal\cavimac\Service;

/**
 * Gestion requête SQL pour les status
 */
class MemberStatusQueryService {


  /**
   * Recupération de tous les statuts
   *
   * @return array - liste des status
   */
  public function findAllStatus() {
    $database = \Drupal::database();
    $query = $database->select('status');
    $query->fields('status', []);
    $result = $query-> execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Récupération d'un statut
   *
   * @return object - status
   */
  public function findStatusById($statusId) {
    //Récupération du membre
    $database = \Drupal::database();
    $findStatus = $database->select('status','status');
    $findStatus
      ->fields('status', []) 
      ->condition('status.id', $statusId, '=');
    $result = $findStatus->execute()->fetchAll(\PDO::FETCH_ASSOC); 
    
    if(count($result) > 0) {
      //si membre trouvé
      $member = (object) $result[0];
      return $member;
    } else {
      return null; 
    }
  }
}