<?php
namespace Drupal\cavimac\Service;

/**
 * Regroupe toutes fonctions à partager entre les services
 */
class UtilityService {  
  /**
   * Trimestre
   */ 
  const TRIMESTER = [
    [
      'janvier',
      'février',
      'mars'

    ],
    [        
      'avril',
      'mai',
      'juin',
    ],
    [        
      'juillet',
      'août',
      'septembre'               
    ],
    [
      'octobre',
      'novembre',
      'décembre' 
    ]
  ];
  
  /**
   * Convertion date format SQL
   * @param string $date Date a convertir
   * @return string $formatDate date avec format SQL
   */
  public function convertDateTimeFormat($date) {    
    $dateArray = explode('/', $date);
    $formatDate = $dateArray[2] .'-'. $dateArray[1] . '-'. $dateArray[0];
    return $formatDate;
  }

  /**
   * Renvoie le nom des colonnes d'une table à partir de la structure de base
   * Les colonnes de nom 'id' ne sont pas pris en compte
   */
  public function exctractFieldColumn($fieldsData) {
    //tableau local de donnée
    $fields = [];

    foreach($fieldsData as $key=> $Parentfields) {        
      if($key === 'fields') {
        foreach($Parentfields as $keyChild => $value) {          
          if($keyChild !== 'id') {            
            $fields [] = $keyChild;
          }          
        }        
      }
    }
    return $fields;
  }

  /**
   * Récupération d'un id a partir d'un text
   * @param array $arrayData - Tableau contenant l'id
   * @param string $arrayKeys - clé recherché
   * @param string $value - text devant renvoyer l'id recherché
   * @return int|null - l'id recherché
   */
  public function getId($arrayData, $arrayKeys, $value) {
    
    foreach($arrayData as $data) {      
      if($data[$arrayKeys]  === $value) {
        return $data['id'];
      }
    }
    return null;
  }

  /**
   * Renvoie le moi à partir des information du trimestre
   *
   * @param string $trimesterInformation - Information sur le trimestre
   * @return string|null - renvoie le mois 
   */
  public function extractDate($trimesterInformation) {
    /**
     * Exemple: 2232
     * 22: année
     * 3: trimestre en cours
     * 2: mois du trimestre en cours
     */

    if(strlen($trimesterInformation) === 4) {
      //Année complète
      $year = substr(date("Y"), 0, 2). substr($trimesterInformation, 0, 2);

      //Numero de trimestre
      $trimester = substr($trimesterInformation, 2, 1);

      //Numéero de mois du timestre
      $monthTrimester = substr($trimesterInformation, 3, 1);

      //Infomation a renoyer
      $dateInformartion = new \stdClass;
      $dateInformartion->year = $year;
      $dateInformartion->month = self::TRIMESTER[$trimester - 1][$monthTrimester - 1];
      return  $dateInformartion;
    }
    return null;
  }
  
}