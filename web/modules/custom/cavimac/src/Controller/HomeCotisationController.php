<?php
namespace Drupal\cavimac\Controller;

/**
 * Accueil Module de cotisation
 */
class HomeCotisationController {

  /**
   * Affichage page principal du module de cotisation
   */
  public function displayHomeCotisationPage() {

    $page [] = [
      '#theme' => 'cavimac_cotisation',
      '#myData' => 'Bonjour'
    ];
    $page['#attached']['library'][] = 'cavimac/cavimac';

    return $page;
  }

}