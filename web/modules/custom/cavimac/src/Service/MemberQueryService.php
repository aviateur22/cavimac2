<?php
namespace Drupal\cavimac\Service;

/**
 * Gestion des requete SQL pour les membres
 */
class MemberQueryService {
  
  /**
   * Récupération information d'un membre par son NIR
   * @param $id - NIR du memnbre
   * @return object|null - données du membre
   */
  public function findMemberById($id) {       
    //Récupération du membre
    $database = \Drupal::database();
    $findMember = $database->select('member','member');
    $findMember->join('member_cotisation', 'member_coti', 'member.nir = member_coti.member_nir');  
    $findMember->join('community', 'community', 'member.community_number = community.community_number');
    $findMember->join('cult', 'cult', 'community.cult_id = cult.id');
    $findMember->join('status', 'status', 'member_coti.status_id = status.id');
    $findMember
      ->fields('member_coti')
      ->fields('member')
      ->fields('community', ['cult_id'])
      ->fields('cult', ['label'])
      ->fields('status', ['label']) 
      ->condition('member.nir', $id, '=');
    $result = $findMember->execute()->fetchAll(\PDO::FETCH_ASSOC); 
    
    if(count($result) > 0) {
      //si membre trouvé
      $member = (object) $result[0];
      return $member;
    } else {
      return null; 
    }
  }
}