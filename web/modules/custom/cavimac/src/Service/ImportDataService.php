<?php
namespace Drupal\cavimac\Service;

/**
 * Traitement import des données fichier TxT
 */
class ImportDataService {
  //array code CTP par collectivité
  protected $communityCtpData = [];  

  //array données collectivité
  protected $communityData = [];

  //array pour stocker les codes postaux et collectivité
  protected $zipData = [];

  //nom des fichiers
  protected $memberFileName = "donnees_LISTNOM.txt";
  protected $slipFileName = "donnees_BRC.txt";
  protected $ctpFileName = "donnees_CTP.txt";
  protected $reductionFileName = "donnees_minoration.txt";
  protected $computationFileName = "donnees_calcul.txt";

  /**
   * Initialisation des services
   * @param \Drupal\cavimac\Service\ImportDataQueryService $importDataQueryService
   * @param \Drupal\cavimac\Service\TableService $tableService
   * @param \Drupal\cavimac\Service\TableService $utilityService
   * @param \Drupal\cavimac\Service\memberStatusService $memberStatusService
   */
  function __construct($importDataQueryService, $tableService, $memberStatusInitializeService, $utilityService) {
    $this->tableService = $tableService;
    $this->importDataQueryService = $importDataQueryService ;  
    $this->memberStatusInitializeService = $memberStatusInitializeService;
    $this->utilityService = $utilityService;
  }

  /**
   * Credential FTP
   */
  public function ftpCredential(){
    return 'ftp://aviateur22:Advency1@localhost/';
  }

  /**
   * Information sur les fichiers
   * @return array $ftpFiles - liste des fichier a charger associé aux spécifications des tables
   */
  public function filesInformation() {   
    //liste des fichiers a récupérer
    $ftpFiles = [
      [
        'fileName'=> $this->ctpFileName,
        'specTable' =>  $this->tableService->specCtpTable()
      ],
      [
        'fileName'=> $this->memberFileName,
        'specTable' => $this->tableService->specMemberTable()
      ],
      [
        'fileName'=> $this->slipFileName,
        'specTable' =>  $this->tableService->specSlipTable()
      ],      
      [
        'fileName'=> $this->reductionFileName,
        'specTable' =>  $this->tableService->specReductionTable()
      ],
      [
        'fileName'=> $this->computationFileName,
        'specTable' =>  $this->tableService->specComputationTable()
      ]
    ];

    return $ftpFiles;
  }

  /**
   * Lecture des fichiers 
   */
  public function readFtpFile() {
    $filesInformation = $this->filesInformation();    
    
    for( $i = 0; $i < count($filesInformation); $i++) {     
      
      $filePath = $this->ftpCredential(). $filesInformation[$i]['fileName'];
      $fileName = $filesInformation[$i]['fileName'];
      $fileSpec = $filesInformation[$i]['specTable']; 
        
      //Fichier inexitant
      if(!file_get_contents($filePath)) {
        return ('Le fichier '. $fileName. ' n\'existe pas');
      }
      
      $array = [];

      if (($handle = fopen($filePath , "r")) !== FALSE) {     
        while (($data = fgetcsv($handle, null, ";")) !== FALSE) { 
          //encodage UTF8 des données
          $data = array_map("utf8_encode", $data);

          //suppression des whitespace          
          $data = $this->trimArrayValue($data);                    
          
          if($fileName === $this->slipFileName) {
          //récupération données pours les: collectivité - ctp_Collectivité - bordereaux 
            $data = $this->extractFromSlipArray($data);
          }        

          $array[] = $data;                
        }
        fclose($handle);
      }
      
      if($fileName === $this->memberFileName) {        
        
        //Mise a jour des données avec id cultes et statuts
        $array = $this->getStatusCultId($array);

        //Extraction des Codes postaux
        $this-> extractZipData($array);

        //calcul du statut initiale des membres
        $this->memberStatusInitializeService->statusInitialization($array);

        //filte les données d'un membre (récupératon nom, prénom, ...)
        $member = $this->filterMemberData($array);

        //Ajout des membres en base de données
        $this->importDataQueryService->insertArray($member, $this->tableService->specMemberTable());

        //filtre les cotisation et statut du membre (récupération cotisation - status)       
        $memberCotisationArray = $this->filterMemberCotisation($array); 

        $this->importDataQueryService->insertArray($memberCotisationArray, $this->tableService->specMemberCotisationTable());
        $this->importDataQueryService->insertArray($memberCotisationArray, $this->tableService->specUpdateMemberCotisationTable());

      } elseif($fileName === $this->slipFileName) { 
        
        //remplacement des Cultes au format Text par leur id
        $this->communityData = $this->getCultId($this->communityData);

        //Insertion des données collectivités        
        $this->importDataQueryService->insertArray($this->communityData, $this->tableService->specCommunityTable());
        
        //Insertion des données collectivité - CTP
        $this->importDataQueryService->insertArray($this->communityCtpData, $this->tableService->specComumunityCtpTable());

        //Insertion des borderaux filtrés
        $this->importDataQueryService->insertArray($array, $fileSpec);  

      } else {
        //Insertion des autres données
        $this->importDataQueryService->insertArray($array, $fileSpec);      
      }
    }
  }

  /**
   * Import des données statiques cultes/statuts
   */
  function importStaticData() {
    //données des cultes
    $this->importDataQueryService->insertArray($this->cult(), $this->tableService->specCultTable()); 
    
    //données des statuts
    $this->importDataQueryService->insertArray($this->status(), $this->tableService->specStatusTable()); 
    
    //données des statuts calculé
    $this->importDataQueryService->insertArray($this->calculatedSatus(), $this->tableService->specCalculatedStatusTable()); 

    //étape de declaration
    $this->importDataQueryService->insertArray($this->stepDeclaration(), $this->tableService->specStepProcess()); 
  }

  /**
   * Extraction de données à partir du fichier bordereaux
   * @param array $dataRow - Ligne du fichier bordereau
   * @return array $data - Données du bordereau filtrées
   */
  public function extractFromSlipArray($dataRow) {
    // Filtre les données liason borderaux et ctp
    $this->filterCtpCommunityData($dataRow);         

    //extraction des données des collectivités
    $this->extractCommunityData($dataRow);

    //Filtre données des bordereaux
    $data = $this->filterSlipData($dataRow);         

    return $data;
  }

  /**
   * Suppression des espaces blanc
   * @param array $rowData - array de donnnées
   * @return array $$rowDataTrim - array trimmé
   */
  function trimArrayValue($rowData) { 
    $rowDataTrim = [];
    foreach($rowData as $item) {
      $trimItem = trim($item);
      $rowDataTrim [] = $trimItem;
    } 
    return $rowDataTrim;
  }  

  /**
   * Filtre les données pour les bordereau
   * @param array $rowData - données a filtrer
   * @return array $filterData - données filtrées des bordereaux
   */
  public function filterSlipData($rowData) {     
    $filterData = [
      //numéro de collectivité
      $rowData[3],

      //trimester
      $rowData[4],

      //date de paiement
      $rowData[5],

      //total cotisation
      $rowData[72],

      //avoir
      $rowData[73], 

      //total paiement
      $rowData[74],
    ];

    return $filterData;
  }

  /**
   * Extraction des données liées aux commune
   * @param array $rowData - données pour extraction des collectivités
   */
  public function extractCommunityData($rowData) {   
    //Vérification si numméro de collectivité deja répertirioé
    $index = array_search($rowData[3], array_column($this->communityData, 0));

    if(!is_int($index)) {
      
      //nom des differents fichiers  et table    
      $this->communityData[] =  [   
        //communityNumber
        $rowData[3],
        
        //Siret     
        $rowData[0],
        
        //account number
        $rowData[2],
        
        //zip      
        $this->findCommunityZipeCode($rowData[3]),
        
        //community name
        $rowData[1],

        //cult
        $rowData[75],

        //étape de decalaration
        '1'
      ];
    }
  }

  /**
   * Extraction des données de liasons code CTP et collectivité
   * @param array $rowData
   */
  public function filterCtpCommunityData($rowData) {    
    
    $ctpArray = [];
    $communityNumber = $rowData[3];
    for( $i = 6; $i < count($rowData) - 5; $i+=6) {

      //Code CTP
      $ctpCode = $rowData[$i];

      //Nombre de personne cotisant à ce code CTP
      $ctpMember = $rowData[$i + 2];

      //Montant global salaires
      $ctpMemberCotisation = $rowData[$i + 3];

      //pourcentage de cotisant
      $ctpPrice = $rowData[$i + 4];

      //Montant de la cotisation
      $ctpTotalCotisation = $rowData[$i + 5];  
      
      //Ajout si code CTP !==0
      if( intval($ctpCode, 10) !== 0) {
        
        $ctpArray [] = [
          $communityNumber, $ctpCode, $ctpMember, $ctpMemberCotisation, $ctpPrice, $ctpTotalCotisation
        ];         
      }
      
    }

    if(count($ctpArray) > 0) {
      foreach($ctpArray as $ctp) {
        $this->communityCtpData []= $ctp;
      }
    }
  }

  /**
   * Filtre les données des membres de collectivités
   * @param array $arrayData - données du membre
   * @return array
   */
  public function filterMemberData($arrayData) {
    $filterMember = [];

    for($i = 0; $i < count($arrayData); $i++) {      

      $rowData = $arrayData[$i];

      //nir memebre
      $nir = $rowData[7];

      //numéro de collectivité
      $communityNumber = $rowData[3];      

      //prénom
      $firstName = $rowData [8];

      //nom de famille
      $lastName = $rowData [9];    

      $filterMember [] = [
        $nir, $communityNumber, $firstName, $lastName
      ];
    };
    //dump($filterMember);
    return $filterMember;
  }

  /**
   * Filtre les données de cotisation d'un memebre
   * @param array $arrayData - donnée du mebre
   * @return array
   */
  public function filterMemberCotisation($arrayData){   
    $filterMember = [];

    for($i = 0; $i < count($arrayData); $i++) {
      $rowData = $arrayData[$i];

      //nir membre
      $memberId = $rowData[7];

      //statut calculé - Récupération de l'id
      $status =  $rowData [10];    
      $rn = $rowData [11];
      $stateRn = $rowData [12]; 
      $rp = $rowData [13]; 
      $stateRp = $rowData [14]; 
      $ro = $rowData [15]; 
      $stateRo = $rowData [16]; 
      $rco = $rowData [17];
      $stateRco = $rowData [18];
      $csg1 = $rowData [19];
      $statecsg1 = $rowData [20];
      $csg2 = $rowData [21];
      $statecsg2 = $rowData [22];
      $csg3 = $rowData [23];
      $statecsg3 = $rowData [24];
      $csg4 = $rowData [25];
      $statecsg4 = $rowData [26];
      $csg5 = $rowData [27];
      $statecsg5 = $rowData [28];
      $rv = $rowData [29];
      $staterv = $rowData [30];

      $filterMember [] = [
        $memberId,      
        $status,
        $rn,
        $stateRn,
        $rp,
        $stateRp,
        $ro,
        $stateRo,
        $rco,
        $stateRco,
        $csg1,
        $statecsg1,
        $csg2,
        $statecsg2,
        $csg3,
        $statecsg3,
        $csg4,
        $statecsg4,
        $csg5,
        $statecsg5,
        $rv,
        $staterv,
      ];
    }  
    
    return $filterMember;
  }  

  /**
   * Extraction des codes postaux des collectivités
   *
   * @param array $arrayData - liste des membres
   * @return void
   */
  public function extractZipData($arrayData) {
    foreach($arrayData as $data) {
      $zipPresent = array_column($this->zipData, 'community_number');
      if(!array_search($data[3], $zipPresent)) {
        $this-> zipData [] = [       
          "community_number" => $data[3],
          "zip" => $data[4],
        ];
      }      
    }
  }

  /**
   * Recherche d'un code postal suivant le numéro d'une collectivité
   *
   * @param string $community_number - numéro de la collectivité
   * @return string - le code postal
   */
  public function findCommunityZipeCode($community_number) {
    $zip = "-";

    //Recherche de la collectivité
    $findCommunity = array_column($this->zipData, 'community_number');
    $communityIndex = array_search($community_number, $findCommunity);      

    //Recherche di code Postale
    if($communityIndex) {
      $zip = $this->zipData[$communityIndex]['zip'];
      //Ne conserve que les 5 premiers chiffre
      if(strlen($zip) > 6 ) {          
        $zip = substr($zip, 0, 5);
      }
    }
    return $zip;
  }

  /**
   * Récupération des Id des cultes
   * @param array $arrayData - Tableau avec la liste des collectivités
   * @return array - Tableau avec la liste des collectivités et id des cultes
   */
  public function getCultId($arrayData) {

    //Récupération des statut calculé
    $cultList = $this->importDataQueryService->selectAll($this->tableService->specCultTable());

    for($i = 0; $i < count($arrayData); $i++) {
      $cultId = $this->getId($cultList, 'label', $arrayData[$i][5]);

      if($cultId === null) {
        dump('Le culte n\'est pas référencé ' . $arrayData[$i][5]);
      }
      $arrayData[$i][5] =$cultId;
    }
    
    return $arrayData;
  }

  /**
   * Recherche des id pour les cultes et statut calculés
   * @param array $arrayData - liste des membres
   * @return array - liste des membres avec les id des cultes et status
   */
  function getStatusCultId($arrayData) {
    //Récupération des statut calculé
    $cultList = $this->importDataQueryService->selectAll($this->tableService->specCultTable());

    //Récupération des statut calculé
    $calculatedStatusList = $this->importDataQueryService->selectAll($this->tableService->specCalculatedStatusTable());

    for($i = 0; $i < count($arrayData); $i++ ) {
      
      //recherche correspondance pour le culte id
      $cultId = $this->utilityService->getId($cultList, 'label', $arrayData[$i][31]);
      if($cultId === null) {
        dump('Pas de correspondance trouvée pour le culte ' . $arrayData[$i][31]);
      } else {
        $arrayData[$i][31] = $cultId;
      }      

      //recherche id du statut calculé     
      $statusId = $this->utilityService->getId($calculatedStatusList, 'label', $arrayData[$i][10]);
      if($statusId === null) {
        dump('Pas de correspondance trouvée pour le statut ' . $arrayData[$i][10]);
      } else {
        $arrayData[$i][10] = $statusId;
      }      
    }

    return $arrayData;
  }

  /**
   * Récupération d'un id a partir d'un text
   * @param array $arrayData - Tableau contenant l'id
   * @param string $arrayKeys - clé recherché
   * @param string $value - text devant renvoyer la clé
   * @return int|null - l'id recherché
   */
  function getId($arrayData, $arrayKeys, $value) {
    
    foreach($arrayData as $data) {      
      if($data[$arrayKeys]  === $value) {
        return $data['id'];
      }
    }
    return null;
  }

  #region cult/status data

  /**
   * Etapes de déclaration 
   */
  function stepDeclaration() {
    return [
      ['vérification des membres'],      
      ['valider le borderau de paiement'],      
      ['valider le paiement'],     
    ];
  }

  /**
   * Statut du membre
   */
  function status() {
    return [      
      ['COTISANT VOLONTAIRE'],      
      ['MINISTRE DU CULTE'],      
      ['MINISTRE DU CULTE - PENSIONNE CAVIMAC'],      
      ['MINISTRE DU CULTE - AA OU PENSIONNE AR'],      
      ['MEMBRE ACCUEILLI RN'],      
      ['MEMBRE ACCUEILLI RP'],      
      ['MEMBRE ACCUEILLI - AA OU PENSIONNE AR'],      
      ['SEMINARISTE - REGIME CAVIMAC'],      
      ['SEMINARISTE - AUTRE REGIME'],      
      ['POSTULANT/NOVICE - REGIME CAVIMAC'],      
      ['POSTULANT/NOVICE - AUTRE REGIME'],      
      ['MINISTRE DU CULTE AVEC TRAITEMENT'],      
      ['MINISTRE DU CULTE SANS TRAITEMENT'],      
      ['MINISTRE DU CULTE AVEC TRT - AA OU P. AR'],      
      ['MINISTRE DU CULTE SANS TRT - AA OU P. AR'],      
      ['MEMBRE RELIGIEUX RN'],      
      ['MEMBRE RELIGIEUX RP'],      
      ['MEMBRE RELIGIEUX - AA OU PENSIONNE AR'],      
      ['MINISTRE DU CULTE SANS ACTIVITE'],  
      ['RADIE']
   ];
  }

  /**
   * Statut calculé du membre
   */
  function calculatedSatus() {
    return [      
      ['COTISANT VOLONTAIRE'],      
      ['MINISTRE DU CULTE'],      
      ['MINISTRE DU CULTE - PENSIONNE CAVIMAC'],      
      ['MINISTRE DU CULTE - AA OU PENSIONNE AR'],      
      ['MEMBRE ACCUEILLI RN'],      
      ['MEMBRE ACCUEILLI RP'],      
      ['MEMBRE ACCUEILLI - AA OU PENSIONNE AR'],      
      ['SEMINARISTE - REGIME CAVIMAC'],      
      ['SEMINARISTE - AUTRE REGIME'],      
      ['POSTULANT/NOVICE - REGIME CAVIMAC'],      
      ['POSTULANT/NOVICE - AUTRE REGIME'],      
      ['MINISTRE DU CULTE AVEC TRAITEMENT'],      
      ['MINISTRE DU CULTE SANS TRAITEMENT'],      
      ['MINISTRE DU CULTE AVEC TRT - AA OU P. AR'],      
      ['MINISTRE DU CULTE SANS TRT - AA OU P. AR'],      
      ['MEMBRE RELIGIEUX RN'],      
      ['MEMBRE RELIGIEUX RP'],      
      ['MEMBRE RELIGIEUX - AA OU PENSIONNE AR'],      
      ['MINISTRE DU CULTE SANS ACTIVITE'],      
      ['RADIE']
   ];
  }
  
  function cult() {
    return [      
      ['DIOCESE'],    
      ['CATHOLIQUE ROMAIN'],    
      ['BOUDDHISTE'],    
      ['ANGLICAN'],    
      ['CATHOLIQUE PRECHALCEDONIEN'],    
      ['MUSULMAN'],    
      ['JUDAIQUE'],    
      ['AUTRES CULTES D\'ASIE'],
      ['ORTHODOXE'],    
      ['PROTESTANT OU EVANGELIQUE'],
      ['CATHOLIQUE NON ROMAIN'],    
      ["CULTES D'INSPIRATION CHRETIENNE"],    
      ['TEMOINS DE JEHOVAH'],    
      ['HINDOUISTE'],    
      ['ADVENTISTE']      
    ];
  }
  #endregion
}