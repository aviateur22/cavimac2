<?php
namespace Drupal\cavimac\Service;

/**
 * Model pour un membre
 */
class MemberModelService {
  //nir du membre
  private $_nir;

  //index du nouveau statut
  private $_statusId;

  //index du nouveau csg
  private $_csgId;

#region getter/setter

  /**
   * NIR
   *   
   */
  public function setNir($nir) {
    $this->_nir = $nir;
  }
  public function getNir() {
    return $this->_nir;
  }

  /**
   * STATUS
   */
  public function setStatus($status) {
    $this->_statusId = $status;
  }
  public function getStatus() {
    return $this->_statusId;
  }

  /**
   * CSG
   */
  public function setCsg($csg) {
    $this->_csgId = $csg;
  }
  public function getCsg() {
    return $this->_csgId;
  }
#endregion


}