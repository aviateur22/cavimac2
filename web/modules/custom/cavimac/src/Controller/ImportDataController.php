<?php
namespace Drupal\cavimac\Controller;

//Import
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Gestion de l'import des données de CAVIMAC
 */
 class ImportDataController extends ControllerBase {

  //service
  protected object $importDataService;

  function __construct() {
    $this-> importDataService = \Drupal::service('cavimac.import_data_service');
  }
  /**
   * Extraction des données txt
   */
  public function importData() {
    try {
      //import des données statiques
      $this-> importDataService-> importStaticData();

      //chargement des fichiers txt
      $this-> importDataService-> readFtpFile();
      
      return new JsonResponse(['status' => 'Les fichiers sont importés avec succés']);

    } catch (\Throwable $th) {
      
      $response = new JsonResponse();
      $response->setStatusCode(500);
      $response->setData([
        'status' => $th->getMessage()
      ]);      
      return $response;      
    }
  }
 }