(function($, Drupal) {

  //uri de base cavimac
  const uri = "/cavimac2/web/";


  /**
   * Chargement des données txt
   */
  $("#load").click(function(event) {
    event.preventDefault();
    
      jQuery.get( uri + 'cotisation/load-data')
      .then(result => {        
        $('.text').text(result.status); 
      })
      .catch(error => {
        console.log(error);
        $('.text').text(error.responseJSON.status); 
      });
  });

  /**
   * Affichage détail d'une personne
   */
  $(".member").click(async function() {

     //nir du membre cliqué
     const nir= $(this).data("nir");    

     //Affichage Modale
    try {
      const ajaxSettings = {
        url: uri + 'cotisation/declaration/community/member/'+ nir,
        dialogType: 'modal',
        dialog: { width: 550 }      
      };
      
      const myAjaxObject = Drupal.ajax(ajaxSettings);      
      const result = myAjaxObject.execute();     
    } catch (error) {
        console.log(error);
    }
  });

  /**
   * Masque/Affiche les radio button CSG disponible pour un statut
   * @param {Array} statusCsg -  liste des CSG disponible pour un statut
   * @param {Array} csgAvailable - liste des CSG disponible
   */
  function csgButtonVisibility (csgStatus, csgAvailable) {
    csgAvailable.forEach(csg => {
      if(csgStatus.findIndex(element => element === csg.name) > -1){        
        csg.csgElement.show();
      } else {
        csg.csgElement.hide();
      }
    });
  }

  /**
   * 
   * @param {Array} csgStatusArray 
   * @param {Array} csgAvailable 
   */
  function defaultCsgCheck(csgStatusArray, csgAvailable) {
    
    defaultCsg = csgAvailable.find(csg => csg.name === csgStatusArray[0]);
    $(`#csg${defaultCsg.name}`).prop('checked', true);
  }

  /**
   * Initilisae les élements HTML a l'ouverture de la modal
   * @param {HTMLElement} csgMessageEmptyElement 
   * @param {HTMLElement} csgMessageSelectElement 
   * @param {Array} csgAvailable - Liste des CSG dospinible par default
   */
  function displayOnLoad(csgMessageEmptyElement, csgMessageSelectElement, csgAvailable) {
    csgMessageEmptyElement.hide();
    csgMessageSelectElement.show();
    
    //Masque tous les boutons de CSG par default
    csgButtonVisibility([], csgAvailable); 

    //uncheck tous les CSG
    $('input[name=csg]').prop('checked',false);
  }

  /**
   * Gestion de l'affichage des messages
   * 
   * @param {string} statusCsgString - CSG disponible pour le statut séléctionné
   * @param {HTMLElement} csgMessageSelectElement
   * @param {HTMLElement} csgMessageEmptyElement 
   */
  function statusSelectionChange(statusCsgString, csgMessageSelectElement, csgMessageEmptyElement) {
    if(statusCsgString === '-') {
      csgMessageSelectElement.hide();
      csgMessageEmptyElement.show();
    } else {
      csgMessageSelectElement.hide();
      csgMessageEmptyElement.hide();              
    }
  }

  /**
   * Statut selection change
   * @param {string} csgStatusString - CSG disponaible sur le statut selectionné
   * @param {HTMLElement} csgMessageSelectElement 
   * @param {HTMLElement} csgMessageEmptyElement 
   * @param {Array} csgAvailable - Liste des CSG dospinible par default
   */
  function statusClick(csgStatusString, csgMessageSelectElement, csgMessageEmptyElement, csgAvailable) {
    let csgStatusArray = [];

    if (csgStatusString !== '-') {
      csgStatusArray = csgStatusString.split(',');
    } 

    //uncheck tous les CSG
    $('input[name=csg]').prop('checked',false);

    //Modification sélection
    statusSelectionChange(csgStatusString, csgMessageSelectElement, csgMessageEmptyElement);            

    //Modification de la visibilité des CSG
    csgButtonVisibility(csgStatusArray, csgAvailable); 
    console.log(csgStatusString, csgStatusArray)
    if(csgStatusArray.length > 0) {
      defaultCsgCheck(csgStatusArray, csgAvailable);
    }
  }

  

  /**
   * Modal modification membre collectivté
   */
  Drupal.behaviors.custom_behavior = {
    attach: function () {
      $(window)
        .once('custom-behavior')
        .on('dialog:aftercreate', function () {          
         
          /**
           * Soumission du formulaire
           */
          $('#member-form').submit(function(event) {            
            event.preventDefault();
            const formData = Object.fromEntries(new FormData(event.target));   

            title = $('span.ui-dialog-title'); 
            title.addClass('header__feedback');        
            if(!formData.status) {             
              title.addClass('error');
              title.text("Selectionner un nouveau statut ");
              return null;
            }
            title.removeClass('error');
            title.text("Données envoyée pour traitement");
            
            console.log(formData);
            jQuery.ajax({
              url: Drupal.url('/cotisation/declaration/community/update-member'),
              type: "PUT",
              dataType: "json",
              data: formData,         
              success: function(result) {
                console.log(result);
              }
            })            
            return false;
          });
          
          //csg de dispobnible
          const csg1 = $('.csg1');
          const csg2 = $('.csg2');
          const csg3 = $('.csg3');
          const csg4 = $('.csg4');
          const csg5 = $('.csg5');          
          csgAvailable = [
            {
              'name': '1',
              'csgElement' : csg1
            },
            {
              'name': '2',
              'csgElement' : csg2
            },
            {
              'name': '3',
              'csgElement' : csg3
            },
            {
              'name': '4',
              'csgElement' : csg4
            },
            {
              'name': '5',
              'csgElement' : csg5
            }
          ]

          //radio statut membre
          const status = $('.status');

          //message pas de CSG de disponible
          const csgMessageEmptyElement = $('.csg_text_empty');

          //message zone CSG pour selection status
          const csgMessageSelectElement = $('.csg_text_select');

          displayOnLoad(csgMessageEmptyElement, csgMessageSelectElement, csgAvailable);

          /**
           * Clique sur un Radio Status
           */
          status.click(function(event) {            
            //csg disponible du statut selectionné
            let csgStatusString = $(this).data("csg"); 
            statusClick(csgStatusString, csgMessageSelectElement, csgMessageEmptyElement, csgAvailable); 
          });
          //
          // var button = $('.updateMember');
          //form.submit(function(event) {            
          //   event.preventDefault();
          //   console.log('ici');
          //   return false;
            
          // }, false);

          // button.click(function(event){  
          //   event.preventDefault();          
          //   jQuery.ajax({
          //     url: Drupal.url('/cotisation/declaration/community/update-member'),
          //     type: "PUT",
          //     dataType: "json",
          //     data: {
          //       message: 'test'
          //     },              
          //     success: function(result) {
          //       console.log(result);
          //     }
          //   })
          // });
        });
    }
  };


})(jQuery, Drupal);