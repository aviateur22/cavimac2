<?php
namespace Drupal\cavimac\Service;

use Error;

/**
 * Service pour mise a jour d'un membre
 */
class MemberUpdateService {
/**
 * 1- on vérifie que l'utilisateur existe
 * 2- on recupere toute ses informations
 * 3- on récupére les nouvelles données
 * 4- on vérifie la cohérence des nouvelles données
 * 4- on mets a jour l'utilisateur:
 *    le statut
 *    les cotisations 
 * 5- on recalcul les cotisation de la collectvité
 */

 /**
  * Membre selectionné
  */
  protected object $member;

  /**
   * Statut selectionné
   */
  protected \stdClass $status;

  /**
   * CSG selectionné
   */
  protected string $csg;
  
  /**
    * Injection des Services
    *
    * @param \Drupal\cavimac\service\UtilityStatusService $utilityStatusService
    * @param \Drupal\cavimac\service\MemberQueryService $memberQueryService
    * @param \Drupal\cavimac\service\MemberModelService $memberModelService
    * @param \Drupal\cavimac\service\MemberStatusQueryService $memberStatusQueryService
    */
  function __construct($utilityStatusService, $memberQueryService, $memberModelService, $memberStatusQueryService) {
    $this->utilityStatusService = $utilityStatusService;
    $this->memberQueryService = $memberQueryService;
    $this->memberModelService = $memberModelService;
    $this->memberStatusQueryService = $memberStatusQueryService;
  }

  /**
    * Récupèration information membre
    *
    * @return void
    */
  public function memberInformation() {
    $findMember = $this->memberQueryService->findMemberById($this->memberModelService->getNir());

    //Membre non présent
    if(!$findMember) {
      throw new \Exception('Unknow member', 404);
    }

    $this->member = $findMember;
    return $findMember->nir;
  }

  /**
   * Récupération des cotisations liées au nouveau statut
   * @return void
   */
  function cotisationStatus() {
    //Récupération des information du membre
    $this->memberInformation();   

    //Récupération des CSG et statuts envoyés
    $this->csg();
    $this->status();

    switch ($this->member->cult_id) {
      //Culte Diocese
      case $this->utilityStatusService::CULT['DIOCESE']:

        //Récupération des cotisations et des CSG pour le statut sélectionné
        $cotisation = $this->findCotisation($this->utilityStatusService::DIOCES_COTISATION, $this->status->id);

        //vérification validité CSG selectionné
        $this-> csgValidity($this->utilityStatusService->extractCsgAvailibility($cotisation), $this->csg);

        //Données des cotisation au format de la base de données
        $cotisationProperties = $this->checkCotisation($cotisation[0]);

        //Update du membre 
        $this->updateMemberStatus($cotisationProperties);

        break;

      //Culte Romain
      case $this->utilityStatusService::CULT['CATHOLIQUE_ROMAIN']:        
        break;

      //Culte Anglican - catholique Préchalcédonien - Musulman - Judaïque - Autres culte d'Asie
      case in_array($this->member->cult_id,
        [
          $this->utilityStatusService::CULT['ANGLICAN'],
          $this->utilityStatusService::CULT['CATHOLIQUE_PRECHALCEDONIEN'], 
          $this->utilityStatusService::CULT['MUSULMAN'], 
          $this->utilityStatusService::CULT['JUDAIQUE'],
          $this->utilityStatusService::CULT['AUTRES_CULTES_D\'ASIE']
        ]):        
        break;

      //Bouddhiste
      case $this->utilityStatusService::CULT['BOUDDHISTE' ]:        
        break;

      //Orthodoxe
      case $this->utilityStatusService::CULT['ORTHODOXE']:        
        break;

      //Protestant - evangelique
      case $this->utilityStatusService::CULT['PROTESTANT_EVANGELIQUE']:        
        break;

      //Catho non romain - Inspiration chrétienne - Témoin de jehovah - Hindouiste
      case in_array($this->member->cult_id, 
        [
          $this->utilityStatusService::CULT['CATHOLIQUE_NON_ROMAIN'],
          $this->utilityStatusService::CULT['CULTES_D\'INSPIRATION_CHRETIENNE'],
          $this->utilityStatusService::CULT['TEMOINS_DE_JEHOVAH'],
          $this->utilityStatusService::CULT['HINDOUISTE'],
        ]):        
        break;

      //Adventiste
      case $this->utilityStatusService::CULT['ADVENTISTE' ]:        
        break;

      default: 
        return null;      
    }
  }

  /**
   * Renvoie le CSG sélectionné
   *
   * @return string - La CSG de sélectionné
   */
  function csg() {
    $csg = $this->memberModelService->getCsg();

    if(empty($csg)) {
      $csg = '-';
    }

    $this->csg = $csg;
  }

  /**
   * Renvoie le statut sélectionné
   *
   * @return int - Id du statut selectionné
   */
  function status() {
    $status = (int) $this->memberModelService->getStatus();      

    if(empty($status)) {
      throw new \Exception('Status selection is mandatory', 400);
    }
    //Info sur le status 
    $statusData = $this->memberStatusQueryService->findStatusById($status);
  
    $this->status = new \stdClass;
    $this->status->id = $status;
    $this->status->label = $statusData->label;
   
    return $status;
  }

  /**
   * Récupération des cotisations disponibles en fonction du statut et du culte
   *
   * @param array $cultCotisation - Liste des statuts disponibles pour un culte
   * @param int $status - Statut selectionné
   * @return array - liste des cotisations disponible pour le statut selectionné
   */
  function findCotisation($cultCotisation, $status) {
    $cotisation = $this->utilityStatusService->findCotisation($cultCotisation, $status);
    if(count($cotisation) === 0) {
      throw new \Exception('This status does not exist for this cult', 400);
    }
    return $cotisation;
  }

  /**
   * Vérification validité CSG sélectionné 
   * 
   * @param string $cotisationCsg - Liste des cotisations disponible pour un statut
   * @param string $csg - CSG sélectionné
   *
   * @return void
   */
  function csgValidity($cotisationCsg, $csg) {    
    $cotisationCsgArray =  substr_count($cotisationCsg, ',') > 0 ? explode(',', $cotisationCsg) : [];

    //Vérification de la CSG        
    if(count($cotisationCsgArray) > 0 && $csg === '-') {
      throw new \Exception('CSG mandatory for this status', 400);
    }

    //Vérification CSG selectionné si le statut possède des CSG
    if(count($cotisationCsgArray) > 0 && !in_array($csg, $cotisationCsgArray)) {
      throw new \Exception('CSG'. $csg .' does not exist in status', 400);
    }

    //Vérification qu'aucun CSG n'est selectionné si le statut ne possède pas de CSG
    if(count($cotisationCsgArray) === 0 && $csg !== '-') {
      throw new \Exception('This status does not have CSG available', 400);
    }
  }

  /**
   * Vérification de chaque TOP des cotisations et mise à jour des états finaux
   * @param string $cotisationString - données de la cotisation
   * 
   * @return object - cotisation avec tous le TOP mis a jour 
   */
  function checkCotisation(string $cotisationString) {

    //cotisation format string en Array 
    $cotisationArray = explode(',', $cotisationString);

    if(count($cotisationArray) !== 10) {
      throw new \Exception('Cotisation data length < 10', 500);
    }

    $newCotisationData = new \stdClass;
    
    //status
    $newCotisationData->status = $this->status->id;
    //top_rn
    $newCotisationData->top_rn = $this->formatCotisation($cotisationArray[0], $this->member->top_rn)->top;

    //state_rn
    $newCotisationData->state_rn = $this->formatCotisation($cotisationArray[0], $this->member->top_rn)->state;

    //top_rp
    $newCotisationData->top_rp = $this->formatCotisation($cotisationArray[1], $this->member->top_rp)->top;

    //state_rp
    $newCotisationData->state_rp = $this->formatCotisation($cotisationArray[1], $this->member->top_rp)->state;

    //top_ro
    $newCotisationData->top_ro = $this->formatCotisation($cotisationArray[2], $this->member->top_ro)->top;

    //state_ro
    $newCotisationData->state_ro = $this->formatCotisation($cotisationArray[2], $this->member->top_ro)->state;

    //top_rco
    $newCotisationData->top_rco = $this->formatCotisation($cotisationArray[3], $this->member->top_rco)->top;

    //state_rco
    $newCotisationData->state_rco = $this->formatCotisation($cotisationArray[3], $this->member->top_rco)->state;

    //top_csg1
    $newCotisationData->top_csg1 = $this->formatCsg(1, $this->member->top_csg1)->top;

    //state_csg1
    $newCotisationData->state_csg1 = $this->formatCsg(1, $this->member->top_csg1)->state;

    //top_csg2
    $newCotisationData->top_csg2 = $this->formatCsg(2, $this->member->top_csg2)->top;
    
    //state_csg2
    $newCotisationData->state_csg2 = $this->formatCsg(2, $this->member->top_csg2)->state;
    
    //top_csg3
    $newCotisationData->top_csg3 = $this->formatCsg(3, $this->member->top_csg3)->top;
    
    //state_csg3
    $newCotisationData->state_csg3 = $this->formatCsg(3, $this->member->top_csg3)->state;

    //top_csg4
    $newCotisationData->top_csg4 = $this->formatCsg(4, $this->member->top_csg4)->top;
 
    //state_csg4
    $newCotisationData->state_csg4 = $this->formatCsg(4, $this->member->top_csg4)->state;

    //top_csg5
    $newCotisationData->top_csg5 = $this->formatCsg(5, $this->member->top_csg5)->top;

    //state_csg5
    $newCotisationData->state_csg5 = $this->formatCsg(5, $this->member->top_csg5)->state;

    //top_rv
    $newCotisationData->top_rv = $this->formatCotisation($cotisationArray[9], $this->member->top_rv)->top;

    //state_rv    
    $newCotisationData->state_rv = $this->formatCotisation($cotisationArray[9], $this->member->top_rv)->state;

    return $newCotisationData;
  }

  /**
   * Renvoie la valeur de la cotisation finale
   *
   * @param string $newCotisationValue
   * @param string $initialCotisationValue
   * @return object - Renvoie le TOP_final et ETAT_final de la nouvelle cotisation
   */
  function formatCotisation(string $newCotisationValue, string $initialCotisationValue) {   

    $newCotisationValue = $newCotisationValue === 'true' ?  'X' : '';

    return $this->format($newCotisationValue, $initialCotisationValue);
  }

  /**
   * Renvoie la valeur du CSG finale
   *
   * @param string $csgId
   * @param string $initialCotisationValue
   * @return object - Renvoie le TOP_final et ETAT_final de la CSG
   */
  function formatCsg(int $csgId, string $initialCsgValue) {   
    
    //Valeur du CSG
    if($this->csg === '-')
      $newCsgValue = '';
    elseif((int)$this->csg === $csgId) {
      $newCsgValue = 'X';
    } else {
      $newCsgValue = '';
    }

    return $this->format($newCsgValue, $initialCsgValue);
  }


  /**
   * Renvoie le STATE et TOP Finale
   *
   * @param string $newValue - Nouveau TOP de la cotisation
   * @param string $initialValue - TOP initial de la cotisation
   * @return object - Renvoie le TOP_FIANL et STATE_FINAL
   */
  function format(string $newValue, string $initialValue) {
     //Format de la nouvelle cotisation    
     $finalCotisation = new \stdClass;

    //Cotisation initial = I
    if($initialValue === 'I') {
      $finalCotisation->top = 'I';
      $finalCotisation->state = 'I';
      return $finalCotisation;
    }

    //Nouvelle cotisation = Cotisation initiale
    if($newValue === $initialValue) {
      $finalCotisation->top = $newValue;
      $finalCotisation->state = 'I';
      return $finalCotisation;
    } 
    
    //Nouvelle cotisation !== Cotisation initiale
    if($newValue !== $initialValue)
    {
      $finalCotisation->top = $newValue;
      $finalCotisation->state = 'M';
      return $finalCotisation;
    }
    throw new \Exception('Cotisation update impossible', 500);
  }

  /**
    * Mise a jour des information du membre
    *
    * @return void
    */
  public function updateMemberStatus($cotisationData) {
    $query = \Drupal::database()->update('update_member_cotisation');
    $query->fields([
      'status_id'=>$cotisationData->status,
      'top_rn' =>$cotisationData->top_rn,
      'state_rn' =>$cotisationData->state_rn,
      'top_rp' =>$cotisationData->top_rp,
      'state_rp' =>$cotisationData->state_rp,
      'top_ro' =>$cotisationData->top_ro,
      'state_ro' =>$cotisationData->state_ro,
      'top_rco' =>$cotisationData->top_rco,
      'state_rco' =>$cotisationData->state_rco,
      'top_csg1' =>$cotisationData->top_csg1,
      'state_csg1' =>$cotisationData->state_csg1,
      'top_csg2' =>$cotisationData->top_csg2,
      'state_csg2' =>$cotisationData->state_csg2,
      'top_csg3' =>$cotisationData->top_csg3,
      'state_csg3' =>$cotisationData->state_csg3,
      'top_csg4' =>$cotisationData->top_csg4,
      'state_csg4' =>$cotisationData->state_csg4,
      'top_csg5' =>$cotisationData->top_csg5,
      'state_csg5' =>$cotisationData->state_csg5,
      'top_rv' =>$cotisationData->top_rv,
      'state_rv' =>$cotisationData->state_rv,

    ]);
    $query->condition('member_nir', $this->memberModelService->getNir());
    $query->execute();
  }  

}