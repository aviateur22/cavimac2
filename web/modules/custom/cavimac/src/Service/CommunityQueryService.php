<?php
namespace Drupal\cavimac\Service;

/**
 * Gestion des requêts SQL pour les collectivités
 */
class CommunityQueryService {

  protected $database;

  function __construct($serviceUtility) {
    $this->database =  \Drupal::database();
    $this->serviceUtility = $serviceUtility;
  }

  /**
   * Renvoie tous les membres d'une collectivité et les informations de la collectivité
   * @param integer $communityId - id de la collectivité
   * @return array|null 
   */
  public function findAllMemberByCommunityId($communityNumber) {    

    //récupération info de la collectivité
    $findCommunity = $this-> database->select('community','c');
    $findCommunity->join('slip', 'slip', 'slip.community_number = c.community_number');
    $findCommunity
    -> fields('c', [])
    -> fields('slip', [])
    -> condition('c.community_number', $communityNumber, '=');
    $result = $findCommunity -> execute()->fetchAll(\PDO::FETCH_ASSOC);

    if(count($result) > 0) {   
         
      //Si collectivité trouvée
      $community = (object) $result[0];
      $community->month = $this->serviceUtility->extractDate($community->trimester)->month;
      $community->year = $this->serviceUtility->extractDate($community->trimester)->year;
      

      //Récupération des membres 
      $findMember = $this-> database->select('member','member');
      $findMember-> join('member_cotisation', 'member_cotisation', 'member.nir = member_cotisation.member_nir');   
      $findMember-> join('community', 'community', 'member.community_number = community.community_number');
      $findMember->join('cult', 'cult', 'community.cult_id = cult.id');
      $findMember->join('status', 'status', 'member_cotisation.status_id = status.id');
      $findMember
      -> fields('member', ['nir', 'last_name', 'first_name'])
      -> fields('member_cotisation', [
        'top_rn', 'top_rp', 'top_ro', 'top_rco', 'top_csg1', 'top_csg2', 'top_csg3', 'top_csg4', 'top_csg5', 'top_rv', 'status_id'])
      -> fields('community', ['cult_id'])
      ->fields('cult', ['label'])
      ->fields('status', ['label']) 
      -> orderBy('member.last_name', 'ASC')
      -> condition('member.community_number', $communityNumber, '=');
      $findMember = $findMember-> execute()-> fetchAll(\PDO::FETCH_ASSOC);
      //renvoie des données
      return [
        'community' => $community,
        'member' => $findMember
      ];

    } else {      
      return null;
    }
  }
}