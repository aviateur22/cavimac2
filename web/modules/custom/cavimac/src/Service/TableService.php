<?php
namespace Drupal\cavimac\Service;

/**
 * Gestion de la création des tables SQL
 */
class TableService {

  //nom des tables SQL
  public $communityTableName = "community";
  public $communityCtpTableName = "community_ctp";
  public $memberTableName = "member";
  public $memberCotisationTableName = "member_cotisation";
  public $updateMemberCotisationTableName = 'update_member_cotisation';
  public $ctpTableName = "ctp";
  public $reductionTableName = "reduction";
  public $slipTableName = "slip";
  public $computationTableName = "base_computation";
  public $declarationStepTableName = "declaration_step";
  public $statusTableName = "status";
  public $calculatedStatusTableName = "calculated_status";
  public $cultTableName = "cult";
  
  /**
   * Liste des différente table SQL nécessaire 
   */
  public function tableInformation() {

    $tableList = [
      //table ctp
      [
        'tableName' => $this-> ctpTableName,
        'spec'=> $this->specCtpTable()
      ],

      //table des cultes
      [
        'tableName' =>  $this-> cultTableName,
        'spec' => $this-> specCultTable()
      ],

      //table des statuts
      [
        'tableName' =>  $this-> statusTableName,
        'spec' => $this-> specStatusTable()
      ],

      //table des statuts calculé
      [
        'tableName' =>  $this-> calculatedStatusTableName,
        'spec' => $this-> specCalculatedStatusTable()
      ],

      //table collectivité
      [
        'tableName' =>  $this-> communityTableName,
        'spec' => $this->specCommunityTable()
      ],

      //table membre
      [
        'tableName' =>  $this-> memberTableName,
        'spec' => $this->specMemberTable()
      ],    
      //table cotisation des membres
      [
        'tableName' => $this-> memberCotisationTableName,
        'spec' => $this -> specMemberCotisationTable()
      ],
      //table update cotisation des membres
      [
        'tableName' => $this-> updateMemberCotisationTableName,
        'spec' => $this -> specUpdateMemberCotisationTable()
      ],
      //table collectivité - CTP
      [
        'tableName' =>  $this-> communityCtpTableName,
        'spec' => $this-> specComumunityCtpTable()
      ],
      //table des minorations
      [
        'tableName' => $this-> reductionTableName,
        'spec' => $this-> specReductionTable()
      ],
      //table des données de calcul
      [
        'tableName' => $this-> computationTableName,
        'spec' => $this-> specComputationTable()
      ],
      //table étape de décaration des cotisations
      [
        'tableName' => $this-> declarationStepTableName,
        'spec' => $this-> specStepProcess()
      ],
      //table bordereau
      [
        'tableName' => $this-> slipTableName,
        'spec' => $this-> specSlipTable()
      ]
    ]; 
    
    return $tableList;
  }
  

  
  #region spécification table
  /**
   * Spec table de étapes de declaration
   * @return array $spec_declaration_step  - spécification de la table déclaration
   */
  public function specStepProcess() {
    $spec_declaration_step = [
      'declaration' => 'cotisation step available',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'not null' => TRUE
        ],
        'label' => [
          'type' => 'varchar',
          'length' => 255
        ]
      ],
      'primary key' => ['id']
    ];
    return $spec_declaration_step;
  }

  /**
   * Spec table membre des collectivités
   * @return array $spec_member - spécification de la table membre
   */
  public function specMemberTable() {
    $spec_member = [
      'description' => 'members data',
      'fields' => [
        'nir' =>[
          'type' => 'varchar',
          'length' => 255,
          'not null' => true

        ],       
        'community_number'=> [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ],
        'first_name' => [
          'type' => 'varchar',
          'length' => 3000,
          'not null' => TRUE,                                
        ],
        'last_name' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,              
        ]
      ],
      'primary key' => ['nir']      
    ];

    return $spec_member;
  }

  /**
   * Spec table des cotisation des membres
   */
  public function specMemberCotisationTable() {
    $spec_member = [
      'description' => 'members cotisation data',
      'fields' => [        
        'member_nir' =>[
          'type' => 'varchar',
          'length' => 255,
          'not null' => true
        ],        
        'status_id'=> [
            'type' => 'varchar',
            'length' => 3
        ],
        'top_rn'=> [
          'type' => 'char', 
          'length' => 1,
        ],
        'state_rn'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_rp'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_rp'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_ro'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_ro'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_rco'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_rco'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg1'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg1'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg2'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg2'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg3'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg3'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg4'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg4'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg5'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg5'=> [
          'type' => 'char', 
          'length' => 1
        ],
        'top_rv'=> [
          'type' => 'char', 
          'length' => 1
        ],
        'state_rv'=> [
          'type' => 'char', 
          'length' => 1
        ]
      ],
      'primary key' => ['member_nir'],
      'foreign keys' => array(
        'member_nir' => array(
          'table' => 'member',
          'columns' => array(
            'member_nir' => 'nir'
          )
        )
      )           
    ];

    return $spec_member;
  }

   /**
   * Spec table des cotisation updates des membres
   */
  public function specUpdateMemberCotisationTable() {
    $spec_member = [
      'description' => 'update members cotisation data',
      'fields' => [        
        'member_nir' =>[
          'type' => 'varchar',
          'length' => 255,
          'not null' => true
        ],        
        'status_id'=> [
            'type' => 'varchar',
            'length' => 3
        ],
        'top_rn'=> [
          'type' => 'char', 
          'length' => 1,
        ],
        'state_rn'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_rp'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_rp'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_ro'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_ro'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_rco'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_rco'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg1'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg1'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg2'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg2'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg3'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg3'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg4'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg4'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'top_csg5'=> [
            'type' => 'char', 
            'length' => 1
        ],
        'state_csg5'=> [
          'type' => 'char', 
          'length' => 1
        ],
        'top_rv'=> [
          'type' => 'char', 
          'length' => 1
        ],
        'state_rv'=> [
          'type' => 'char', 
          'length' => 1
        ]
      ],
      'primary key' => ['member_nir'],
      'foreign keys' => array(
        'member_nir' => array(
          'table' => 'member',
          'columns' => array(
            'member_nir' => 'nir'
          )
        )
      )           
    ];

    return $spec_member;
  }

  /**
   * Spec Table code CTP
   * @return array $spec_ctp - spécification de la table ctp
   */
  public function specCtpTable() {
    $spec_ctp = [
      'description' =>'ctp information',
      'fields' => [      
        'code' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'small',
          'not null' => TRUE,
        ],
        'label'=> [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ],
        'rate'=> [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'small', 
          'not null' => TRUE,
        ],
        'issue_date'=> [
          'type' => 'varchar',
          'mysql_type' => 'datetime',
          'not null' => TRUE
        ]
      ],
      'primary key' => ['code'],
    ];
    return $spec_ctp;
  }

  /**
   * spec table collectivité
   * @return array  $spec_community  - spécification pour la table collectivité
   */
  public function specCommunityTable() {
    $spec_community = [
      'description' => 'community data',
      'fields' => [
        'community_number' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ],
        'siret' => [
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE
        ],
        'account' => [
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE
        ],
        'zip' => [
          'type' => 'varchar',
          'length' => '6',
          'not null' => TRUE
        ],        
        'community_name' => [
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE
        ],
        'cult_id' => [
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE
        ],
        'declaration_step_id' => [
          'type' => 'int',
        ],
  
      ],
      'primary key' => ['community_number'],
      'foreign keys' => [
        'declaration_step_id' => [
          'table' => 'declaration_step',
          'column' => 'id'
        ]
      ]
    ];  
    return $spec_community;
  }

  /**
   * spec table de liaison collectivité - code CTP
   * @return array $spec_community_ctp - spécification table de liaison 
   */
  public function specComumunityCtpTable() {
    $spec_community_ctp = [
      'description' => 'community and ctp relation for slip declaration',
      'fields' => [
        'id' => [
          'type' =>'serial',
          'not null' => TRUE        
        ],
        'community_id' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ],
        'ctp_id' => [
          'type' =>'int',
          'not null' => TRUE
        ],
        'member_quantity_ctp'=> [
          'type' => 'int',
          'size' => 'medium',
          'unsigned' => TRUE
        ],
        'calculate_member_income' => [
          'type' => 'int',    
          'size' => 'medium'
        ],
        'ctp_price'=> [
          'type' => 'int',
          'size' => 'normal'
        ],
        'cotisation_total_price'=> [
          'type' => 'int',
          'size' => 'normal'
        ]
      ],
      'primary key' => ['id'],
      'foreign keys' => [
        'ctp_id' => [
          'table' => 'ctp',
          'column' => 'code'
        ],
        'community_id' => [
          'table' => 'community',
          'column' => 'community_number'
        ]
      ]
    ];
    return $spec_community_ctp;
  }
  
  /**
   * Spec table données de calcul
   * @return array  $spec_base_computation - spécification pour la table calcul
   */
  public function specComputationTable() {
    $spec_base_computation = [
      'description' => 'data to calculate cotisation',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'not null'=> true
        ],
        'name'=> [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ],
        'label'=> [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ],
        'amount'=> [
          'type' => 'int',
          'unsigned'=> TRUE,
          'size' => 'normal',
          'not null' => TRUE
        ]
        ],
        'primary key' => ['id'],
    ];
    return $spec_base_computation;

  }

  /**
   * Spec table créduction
   * @return array $spec_reduction - spécification pour la table minoration
   */
  public function specReductionTable() {
    $spec_reduction = [
      'description'=>'reduction amount for community',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'not null' => TRUE,
        ],
        'community_id' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'medium',
          'not null' => TRUE
        ],
        'ctp_id' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'small',
          'not null' => TRUE,
        ],
        'reduction_label' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ],
        'reduction_amount' => [
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE
        ]      
      ],
      'primary key' => ['id'],
      'foreign keys' => [
        'ctp_id' => [
          'table' => 'ctp',
          'column' => 'code'
        ]
      ]
    ];

    return $spec_reduction;
  }

  /**
   * Spec table boredereau
   * @return array $spec_slip - spécification pour la table bordereau
   */
  public function specSlipTable() {
    $spec_slip = [
      'description' => 'slip data',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'not null' => TRUE,
        ],
        'community_number'=> [
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 255
        ],         
        'trimester'=> [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'medium'
        ],
        'due_date'=> [
          'type' => 'varchar',
          'mysql_type' => 'datetime',
        ],  
        'total_cotisation'=> [
          'type' => 'int',
        ],
        'avail_credit'=> [
          'type' => 'int',
        ],
        'total_payment' => [
          'type' => 'int',
        ]
      ],
      'primary key' => ['id']
    ];    
    return $spec_slip;
  }

  /**
   * Specification de la table statut
   * @return array $spec_status - spécification pour la table statut
   */
  public function specStatusTable() {
    $spec_status = [
      'description'=>'status for member',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'not null' => TRUE,
        ],
        'label' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ]      
      ],
      'primary key' => ['id'],
    ];

    return $spec_status;
  }

  /**
   * Specification de la table statut
   * @return array $spec_status - spécification pour la table statut
   */
  public function specCalculatedStatusTable() {
    $spec_status = [
      'description'=>'calculated status for member',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'not null' => TRUE,
        ],
        'label' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ]      
      ],
      'primary key' => ['id'],
    ];

    return $spec_status;
  }

  /**
   * spécification ppour la table de culte
   * @return array $spec_cult - spécification pour la table de culte
   */
  public function specCultTable() {
    $spec_cult = [
      'description'=>'cult for community',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'not null' => TRUE,
        ],
        'label' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE
        ]      
      ],
      'primary key' => ['id'],
    ];

    return $spec_cult;
  }
  #endregion

}