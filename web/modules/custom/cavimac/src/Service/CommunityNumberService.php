<?php
namespace Drupal\cavimac\Service;

/**
 * Renvoie le numéro de la collectivité
 */
class CommunityNumberService{

  /**
   * Récupération du numéro de la collectivité
   */
  function communityNumber() {
    $user = \Drupal::currentUser()->id();
    $account = \Drupal\user\Entity\User::load($user);
    $name = $account->get('name')->value;

    if(is_null($name)) {      
      return null;
    } else if($name=== 'aviateur22'){
      return '00002';
    }
    else {
      //attribution du numéro de collectivité
      return $name;
    }
  }

}