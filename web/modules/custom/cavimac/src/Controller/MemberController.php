<?php
namespace Drupal\cavimac\Controller;

//import Core/Symfony
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

//Import services
use Drupal\cavimac\Service\MemberQueryService;
use Drupal\cavimac\Service\CommunityNumberService;
use Drupal\cavimac\Service\CotisationCalculationService;
use Drupal\cavimac\Service\MemberStatusAvailibilityService;
use Drupal\cavimac\Service\MemberUpdateService;
use Drupal\cavimac\Service\UtilityStatusService;
use Drupal\cavimac\Service\MemberModelService;


/**
 * Gestion des membres
 */
class MemberController extends ControllerBase {

  //Numéro de collectivité
  protected string $communityNumber;
  
  /**
   * Injection des services
   *
   * @param MemberQueryService $MemberQueryService
   * @param CommunityNumberService $communityNumberService
   * @param CotisationCalculationService $cotisationCalculationService
   * @param MemberStatusAvailibilityService $memberStatusAvailibilityService
   * @param MemberUpdateService $memberUpdateService
   * @param UtilityStatusService $utilityStatusService
   * @param MemberModelService $memberModelService
   */
  function __construct(
    MemberQueryService $MemberQueryService,    
    CommunityNumberService $communityNumberService,
    CotisationCalculationService $cotisationCalculationService,
    MemberStatusAvailibilityService $memberStatusAvailibilityService,
    MemberUpdateService $memberUpdateService,
    UtilityStatusService $utilityStatusService,
    MemberModelService $memberModelService
    ) {
    
    $this->memberQueryService =  $MemberQueryService;
    $this->communityNumberService = $communityNumberService;
    $this->cotisationCalculationService = $cotisationCalculationService;    
    $this->memberStatusAvailibilityService = $memberStatusAvailibilityService;
    $this->memberUpdateService = $memberUpdateService;
    $this->utilityStatusService = $utilityStatusService;
    $this->memberModelService = $memberModelService;

   //récupération N° collectivité
   $this->communityNumber = $this->communityNumberService-> communityNumber();
  }

  /**
   * Injection des Services CAVIMAC
   *
   * @param ContainerInterface $container
   * @return void
   */
  public static function create(ContainerInterface $container) {
    return new static(
     $container->get('cavimac.member_query_service'),
     $container->get('cavimac.community_number_service'),
     $container->get('cavimac.cotisation_service'),
     $container->get('cavimac.member_status_availibility_service'),
     $container->get('cavimac.member_update_service'),     
     $container->get('cavimac.utility_status_service'),
     $container->get('cavimac.member_model_service'),
    );
 }

  /**
   * Recherche des informations d'un membre
   * @param string $nir - référence du membre 
   * @param string $communityNumber - référence de la collectivité
   */
  public function displayMemberById($nir) {
    try  {
      //Information sur le membre sélectionné
      $member = $this-> memberQueryService-> findMemberById($nir);
      
      // Récupération des status et csg disponible pour un membre
      //$status = $this-> memberStatusService-> statusAvailable((int)$member->cult, (int)$member->status);
      $status = $this-> memberStatusAvailibilityService-> statusAvailibility((int)$member->cult_id, (int)$member->status_id);

      if(is_null($member)){
        // pas de membre
        $page [] = [
          '#theme' => 'cavimac-member',
          '#errorMessage' => 'Ce membre n\'est pas référencé en base de donnée',
        ];
        $page['#attached']['library'][] = 'cavimac/cavimac';          
        return $page;
      } else if ($member->community_number !== $this-> communityNumber) {
        //incohérence numéro de collectivité
        $page [] = [
          '#theme' => 'cavimac-member',
          '#errorMessage' => 'Ce membre n\'appartient pas à votre collectivité',
        ];
        $page['#attached']['library'][] = 'cavimac/cavimac';        
        return $page;
      } else {      
        $page [] = [
          '#theme' => 'cavimac-member',
          '#status' => $status,
          '#member' => $member,      
        ];
        $page['#attached']['library'][] = 'cavimac/cavimac';
        return $page;
      }    
    } catch (\Throwable $th) {
      
      $response = new JsonResponse();
      $response->setStatusCode(500);
      $response->setData([
        'status' => $th->getMessage()
      ]);
      //dump($response);
      return $response;      
    }
  }

  /**
   * Ajout d'un membre dans une collectivité
   */
  public function addMember() {

  }

  /**
   * Mise a jour d'un membre d'une collectivité
   */
  public function updateMemberById()  {
    try {         
      //Récupération données de la requete 
      $nirNumber =  \Drupal::request()->get('nir');
      $statusIndex = \Drupal::request()->get('status');
      $csgIndex = \Drupal::request()->get('csg');   

      $this->setMemberModel($nirNumber, $statusIndex, $csgIndex);
      $this->memberUpdateService->cotisationStatus();
      return new JsonResponse(
        [
          'status' => 'les données envoyées sont: '      
        . 'le statut: ' . $statusIndex . ' - ' 
        . 'la CSG: ' . $csgIndex . ' - ' 
        . 'NIR: ' . $nirNumber . ' - ' 
       // . $this->memberUpdateService->statusInformation() . '-' 
        //. $this->memberUpdateService->memberInformation()
      ]);

    } catch (\Throwable $th) {
      
      $response = new JsonResponse();     
      $response->setStatusCode($th->getCode() === 0 ? 500 : $th->getCode());
      $response->setData([
        'status' => $th->getMessage()
      ]);
      //dump($response);
      return $response;      
    }
  }

  /**
   * Passe un membre à radié
   */
  public function deleteMemberById() {

  }

  /**
   * Instancie le modele Membre
   *
   * @param string $nirNumber
   * @param string $statusIndex
   * @param string $csgIndex
   * @return void
   */
  private function setMemberModel($nirNumber, $statusIndex, $csgIndex) {
    $this->memberModelService->setNir($nirNumber);
    $this->memberModelService->setStatus($statusIndex);
    $this->memberModelService->setCsg($csgIndex);
  }
}