<?php
namespace Drupal\cavimac\Service;

/**
 * Requete SQL 
 */
class TableQueryService {

  protected $database;
  protected $schema ;

  /**
   * Initilisation des services
   * @param \Drupal\cavimac\Service\TableService $tableService 
   */
  function __construct($tableService) {
    $this-> tableService = $tableService;
    $this-> database = \Drupal::database();   
    $this-> schema = $this-> database-> schema();
  }
  /**
   * création des tables SQL nécessaire au module
   */
  public function createTable() {
    //Récupération des tables a créer
    $tableList = $this-> tableService-> tableInformation();

    foreach($tableList as $table){
      $tableName = $table['tableName'];
      $specTable = $table['spec'];
      $this-> schema-> createTable($tableName, $specTable); 
    }
  }

  /**
   * Suppression des table SQL
   */
  public function deleteTable() {
    //Récupération des tables a créer
    $tableList = $this-> tableService-> tableInformation();

    foreach($tableList as $table){
      $tableName = $table['tableName'];
      $this-> schema-> dropTable($tableName);
    }
  }
}