<?php
namespace Drupal\cavimac\Service;

/**
 * Récupération des statuts disponible pour la modification d'un membre
 */
class MemberStatusAvailibilityService {
    
    /**
   * 1- Récupérer toutes les données actuelles du membre (status calculé, statut initial, cotisation, csg....)
   * 2- En fonction du statut du membre récupérer:
   * - Les nouveaux statuts disponibles
   * - les CSG liées aux nouveaux statuts
   * - "le statuts calculé" lié aux statuts
   * 3- Envoyer ces données vers le Front pour affichage et selections
   * 4- Renvoyer du Front-> Back le nouveau statut selectionné - ainsi que les CSG
   * 5- Si modification du statut: 
   * - Mettre a jours les les ETAT_RN, TOP_RN, ETAR_RP, TOP_RP ...., statut calculé et statut 
   * - Passer à la modification de la cotisation (CotisationCalculation.php)
   * 6- Sauvgarder les nouvelles données du membre
   */

   //Liste des statues
   protected $statusList;



   /**
    * Injection des services
    *
    * @param \Drupal\cavimac\Service\UtilityStatusService $utilityStatusService
    * @param \Drupal\cavimac\Service\ImportDataQueryService $importDataQueryService
    * @param \Drupal\cavimac\Service\TableService $tableService
    */
   function __construct($utilityStatusService, $importDataQueryService, $tableService) {
    $this->utilityStatusService = $utilityStatusService;
    $this->importDataQueryService = $importDataQueryService ;   
    $this->tableService = $tableService; 

    //récupération des statuts initiaux
    $this->statusList = $this->importDataQueryService-> selectAll($this->tableService-> specStatusTable());
    
   }

   /**
    * Undocumented function
    *
    * @param string $memberId - 
    * @return void
    */
   public function statusAvailibility($cultIndex, $statusIndex) {  
    
    switch ($cultIndex) {
      //Culte Diocese
      case $this->utilityStatusService::CULT['DIOCESE']:  
        
        return $this->findStatus($this->utilityStatusService::STATUS_DIOCES_MODIFICATION, $statusIndex, $this->utilityStatusService::DIOCES_COTISATION);
        break;

      //Culte Romain
      case $this->utilityStatusService::CULT['CATHOLIQUE_ROMAIN']:
        
        return $this->findStatus($this->utilityStatusService::CATHO_ROM_MODIFICATION, $statusIndex, $this->utilityStatusService::CATHO_ROMAIN_COTISATION);
        break;

      //Culte Anglican - catholique Préchalcédonien - Musulman - Judaïque - Autres culte d'Asie
      case in_array($cultIndex,
        [
          $this->utilityStatusService::CULT['ANGLICAN'],
          $this->utilityStatusService::CULT['CATHOLIQUE_PRECHALCEDONIEN'], 
          $this->utilityStatusService::CULT['MUSULMAN'], 
          $this->utilityStatusService::CULT['JUDAIQUE'],
          $this->utilityStatusService::CULT['AUTRES_CULTES_D\'ASIE']
        ]):
        
        return $this->findStatus($this->utilityStatusService::ANGLICAN_MODIFICATION, $statusIndex, $this->utilityStatusService::ANGLICAN_COTISATION);
        break;

      
      
      //Bouddhiste
      case $this->utilityStatusService::CULT['BOUDDHISTE' ]:
        
        return $this->findStatus($this->utilityStatusService::BOUDDHISTE_MODIFICATION, $statusIndex, $this->utilityStatusService::BOUDDHISTE_COTISATION);
        break;

      //Orthodoxe
      case $this->utilityStatusService::CULT['ORTHODOXE']:
        
        return $this->findStatus($this->utilityStatusService::ORTHO_MODIFICATION, $statusIndex, $this->utilityStatusService::ORTHODOX_COTISATION);
        break;

      //Protestant - evangelique
      case $this->utilityStatusService::CULT['PROTESTANT_EVANGELIQUE']:
        
        return $this->findStatus($this->utilityStatusService::PROT_EVAN_MODIFICATION, $statusIndex, $this->utilityStatusService::PROT_EVAN_COTISATION);
        break;

      //Catho non romain - Inspiration chrétienne - Témoin de jehovah - Hindouiste
      case in_array($cultIndex, 
        [
          $this->utilityStatusService::CULT['CATHOLIQUE_NON_ROMAIN'],
          $this->utilityStatusService::CULT['CULTES_D\'INSPIRATION_CHRETIENNE'],
          $this->utilityStatusService::CULT['TEMOINS_DE_JEHOVAH'],
          $this->utilityStatusService::CULT['HINDOUISTE'],
        ]):
        
        return $this->findStatus($this->utilityStatusService::CATHO_NON_ROM_MODIFICATION, $statusIndex, $this->utilityStatusService::CATHO_NON_ROMAIN_COTISATION);
        break;

      //Adventiste
      case $this->utilityStatusService::CULT['ADVENTISTE' ]:
        
        return $this->findStatus($this->utilityStatusService::ADVENTISTE_MODIFICATION, $statusIndex, $this->utilityStatusService::ADVENTISTE_COTISATION);
        break;

      default: 
        return null;      
    }   
   }

   /**
    * Récupération des statuts disponible suivant le culte et le statut actuel du membre
    *
    * @param array $cultAvailableStatus - Liste de tous les statuts disponible
    * @param int $statusIndex - Statut actuel du membre
    * @return void
    */
   function findStatus($cultAvailableStatus, $statusIndex, $cultCotisations) {
    foreach($cultAvailableStatus as $key => $value) {
      if($key === $statusIndex) {                
        return $this->extractStatus($cultAvailableStatus[$key], $cultCotisations);
      }
    }    
   }

   /**
    * Récupération des statuts et CSG disponible suivant le culte / statuts d'un membre
    *
    * @param array $statutsArray - Liste des statuts disponible suivant le culte / statuts d'un membre
    * @return void
    */
    function extractStatus($statutsArray, $cultCotisations) {
      $statusList = [];

      foreach($statutsArray as $status) { 
        $statusInformation = $this->extractStatusInformation($status);         
        $csg = $this->utilityStatusService->extractCsgAvailibility($this->utilityStatusService->findCotisation($cultCotisations, $status));

        $statusList[] = [
          'statusId' => $statusInformation['id'],
          'statusLabel' => $statusInformation['label'],
          'csg' => $csg
        ];   
      }   

      return $statusList;
    }

  /**
   * Récupération du nom d'un statut et son id
   *
   * @param int $statusIndex - index du statut
   * @return array - Liste avec id du statut et son nom
   */
  function extractStatusInformation($statusIndex) {
    return $this-> statusList[$statusIndex - 1];
  }
}