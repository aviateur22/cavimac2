<?php
namespace Drupal\cavimac\Service;

/**
 * Gestion des requêts SQL pour import de données
 */
class ImportDataQueryService {
    
  /**
   * Inintialisation des Service
   * @param \Drupal\cavimac\service\UtilityService $utilityService
   * @param \Drupal\cavimac\service\TableService $tableService
   */
  function __construct($utilityService, $tableService) {
    $this-> utilityService = $utilityService;
    $this-> tableService = $tableService;    
  }

  /**
   * Insertion Array
   * @param array $arrayData - données a insérer
   * @param array $specTable - spécification de la table 
   */
  public function insertArray($arrayData, $specTable) {
    //récupérartion des tables sql  
    $tablesList = $this-> tableService-> tableInformation();
    
    //vérification existance des spécification de la table
    $index = array_search($specTable, array_column($tablesList, 'spec'));    
    
    if(is_int($index)) {
      $tableName = $tablesList[$index]['tableName'];

      //suppression des anciennes données
      $this-> deleteDataTable($tableName);
   
      //récupération des colonnes de table
      $fields = $this-> utilityService-> exctractFieldColumn($specTable);
      //Import des données si cohérence entre nombre colonnes dans la table et données
      if(count($arrayData[0]) === count($fields) ) {
        $database = \Drupal::database();   
      
        for($i = 0; $i < count($arrayData ); $i+=1000) {              
         $query = $database->insert($tableName)->fields($fields);      
         $arraySlice = array_slice($arrayData, $i, 1000);
   
         //traitement de la données pour insertion en base de données
         $this->chunckArray($query, $arraySlice, $fields);
         $query->execute();         
      }
     } else {
      throw new \Exception($specTable. '-'. $arrayData[0] . "Import data and SQL table  don't have same structure", 500);
     } 
    } else {
      throw new \Exception("Spec table undefined", 500);
    }
  }

  /**
   * Création de la requete d'insertion pour un Array
   * @param drupal.query $query - query
   * @param array $dataArray - données a insérer
   * @param array $fields - liste des colonne de la table
   */
  public function chunckArray($query, $dataArray, $fields) {
    foreach ($dataArray as $row) {    
      foreach($row as $index => $item) {
        //convertion au format date
        $item = $item !== null ? $item : '';

        if(str_contains($fields[$index], 'date')) {
          $item = $this-> utilityService-> convertDateTimeFormat($item);
        }    
        
        $array[$fields[$index]] = $item; 
      }
      
      $query->values($array);   
      $array =Array();           
    }
    
    return $query;
  }

  /**
  * Suppression complète des données d'une table
  *@param string $tableName Nom de la table
  */
  public function deleteDataTable($tableName) {
    $database = \Drupal::database();  
    $connection = \Drupal::service('database');

    //suppression des données
    $connection-> delete($tableName)
    ->execute();

    //reset auto-increment
    $database->query("ALTER TABLE $tableName AUTO_INCREMENT=1");

  }

  /**
   * Récupération d'une liste de données
   * @param string $specTable - Spécification de la table
   */
  public function selectAll($specTable) {
    //récupérartion des tables sql  
    $tablesList = $this-> tableService-> tableInformation();

    //vérification existance des spécification de la table
    $index = array_search($specTable, array_column($tablesList, 'spec'));    
    
    if(is_int($index)) {
      $tableName = $tablesList[$index]['tableName'];
      $database = \Drupal::database();
      $query = $database->select($tableName);
      $query->fields($tableName, []);
      $result = $query-> execute()->fetchAll(\PDO::FETCH_ASSOC);
      return $result;
    }    
    return null;
  }
}