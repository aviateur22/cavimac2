<?php
namespace Drupal\cavimac\Controller;

/**
 * Gestion des collectivités
 */
class CommunityController {
  // Numéro de collectivité
  protected $communityNumber;

  //service
  protected object $communityQueryService;
  protected object $communityNumberService;

  function __construct() {   
    $this-> communityQueryService = \Drupal::service('cavimac.Community_query_service');
    $this-> communityNumberService = \Drupal::service('cavimac.community_number_service');

    //récupération N° collectivité
    $this-> communityNumber = $this-> communityNumberService-> communityNumber();
  }

 

  /**
   * Récupération des information sur les membres d'une collectivité
   * @param integer $communityNumber - numéro de la collectivité
   * @return array
   */
  public function displayCommunityMembersById() {
    //select 
    $result = $this-> communityQueryService-> findAllMemberByCommunityId($this-> communityNumber);
    
    if(is_null($result)){      
      $page [] = [
        '#theme' => 'cavimac-members',
        '#errorMessage' => 'Cette collectivité n\'existe pas',      
      ];

      $page['#attached']['library'][] = 'cavimac/cavimac';          
      return $page;
    }    
    $page [] = [
      '#theme' => 'cavimac-members',
      '#members' => $result['member'],
      '#community' => $result['community']
    ];
    $page['#attached']['library'][] = 'cavimac/cavimac';

    return $page;
  }

}