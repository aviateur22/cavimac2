<?php
namespace Drupal\cavimac\Service;

/**
 * Regroupe tous les items commun au MemberSatusService
 */
class UtilityStatusService {

#Region Liste STATUS / CULTE / CSG
  const STATUS = [
    'COTISANT_VOLONTAIRE' => 1,
    'MINISTRE_DU_CULTE' => 2,
    'MINISTRE_DU_CULTE_P_CAVIMAC' => 3,
    'MINISTRE_DU_CULTE_AA_P_AR' => 4,
    'MEMBRE_ACCUEILLI_RN' => 5,
    'MEMBRE_ACCUEILLI_RP' => 6,
    'MEMBRE_ACCUEILLI_AA_P_AR' => 7,
    'SEMINARISTE_REGIME_CAVIMAC' => 8,
    'SEMINARISTE_AUTRE_REGIME' => 9,
    'POSTULANT_NOVICE_REGIME_CAVIMAC' => 10,
    'POSTULANT_NOVICE_AUTRE_REGIME' => 11,    
    'MINISTRE_DU_CULTE_AVEC_TRAITEMENT' => 12,
    'MINISTRE_DU_CULTE_SANS_TRAITEMENT' => 13,
    'MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR' => 14,
    'MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR' => 15,
    'MEMBRE_RELIGIEUX_RN' => 16,
    'MEMBRE_RELIGIEUX_RP'=> 17,
    'MEMBRE_RELIGIEUX_AA_P_AR' => 18,
    'MINISTRE_DU_CULTE_SANS_ACTIVITE' => 19,
    'RADIE' => 20
  ];

  // Constante pour les cultes
  const CULT = [
    'DIOCESE' => 1,
    'CATHOLIQUE_ROMAIN' => 2,
    'BOUDDHISTE' => 3,
    'ANGLICAN' => 4,
    'CATHOLIQUE_PRECHALCEDONIEN' => 5,
    'MUSULMAN' => 6,
    'JUDAIQUE' => 7,
    'AUTRES_CULTES_D\'ASIE' => 8,
    'ORTHODOXE' => 9,
    'PROTESTANT_EVANGELIQUE' => 10,
    'CATHOLIQUE_NON_ROMAIN' => 11,
    'CULTES_D\'INSPIRATION_CHRETIENNE' => 12,
    'TEMOINS_DE_JEHOVAH' => 13,
    'HINDOUISTE' => 14,
    'ADVENTISTE' => 15
  ]; 
#endregion

#Region Cotisation CULTE / STATUT
  // Cotisations disponible pour les DIOCES
  const DIOCES_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'],
    [
      self::STATUS['MINISTRE_DU_CULTE'] => 'true,false,true,true,true,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE'] => 'true,false,true,true,false,true,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE'] => 'true,false,true,true,false,false,true,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_ACCUEILLI_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_ACCUEILLI_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_ACCUEILLI_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_P_CAVIMAC'] => 'false,false,false,false,false,false,false,true,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_P_CAVIMAC'] => 'false,false,false,false,false,false,false,false,true,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,true,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,false,true,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,false,false,true,false,false,false'
    ],
    [
      self::STATUS['SEMINARISTE_REGIME_CAVIMAC'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['SEMINARISTE_AUTRE_REGIME'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  // Cotisations disponible pour le culte CATHO_ROMAIN
  const CATHO_ROMAIN_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN' ] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['POSTULANT_NOVICE_REGIME_CAVIMAC'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['POSTULANT_NOVICE_AUTRE_REGIME'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  // Cotisations disponible pour le culte ANGLICAN
  const ANGLICAN_COTISATION = [     
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  // Cotisations disponible pour le culte CATHOLIQUE_PRECHALCEDONIEN
  const CATHOLIQUE_PRECHALCEDONIEN = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  // Cotisations disponible pour le culte MUSULMAN
  const MUSULMAN_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  // Cotisations disponible pour le culte JUDAIQUE
  const JUDAIQUE_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  // Cotisations disponible pour le culte AUTRES_CULTES_DASIE
  const AUTRES_CULTES_DASIE_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  //Cotisation disponible pour le culte BOUDDHISTE
  const BOUDDHISTE_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ], 
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  //Cotisation disponible pour le culte ORTHODOX
  const ORTHODOX_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP']=> 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  //Cotisation disponible pour les cultes PROTESTANT ou EVANGELIQUE
  const PROT_EVAN_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] =>'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  //Cotisation disponible pour le culte  CATHO NON RAMAIN
  const CATHO_NON_ROMAIN_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  //Cotisation disponible pour les cultes D'inspiration CHRE
  const CULTES_INSPIRATION_CHRETIENNE = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  //Cotisation disponible pour le Témoin de Jehovah
  const TEMOINS_DE_JEHOVAH_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  //Cotisation disponible pour le culte HINDOUISTE
  const HINDOUISTE_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];

  //Cotisation disponible pour le culte ADVENTISTE
  const ADVENTISTE_COTISATION = [
    [
      self::STATUS['COTISANT_VOLONTAIRE'] => 'false,false,false,false,false,false,false,false,false,true'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 'true,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 'false,true,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 'false,false,true,false,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 'true,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['MEMBRE_ACCUEILLI_AA_P_AR'] => 'false,false,true,true,false,false,false,false,false,false'
    ],
    [
      self::STATUS['RADIE'] => 'false,false,false,false,false,false,false,false,false,false'
    ]
  ];
#Endregion

#Region modification StatutDisponible 

  //Statuts disponible pour un membre de culte Dioces
  const STATUS_DIOCES_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
    [
      self::STATUS['MINISTRE_DU_CULTE'],
      self::STATUS['MINISTRE_DU_CULTE_P_CAVIMAC'],
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
      self::STATUS['RADIE']
    ],
    self::STATUS['MINISTRE_DU_CULTE'] => 
    [
      self::STATUS['MINISTRE_DU_CULTE_P_CAVIMAC'],
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
      self::STATUS['COTISANT_VOLONTAIRE'],
      self::STATUS['RADIE']
    ],
    self::STATUS['MINISTRE_DU_CULTE_P_CAVIMAC'] => 
    [      
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
      self::STATUS['RADIE']
    ],
    self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 
    [      
      self::STATUS['MINISTRE_DU_CULTE_P_CAVIMAC'],
      self::STATUS['RADIE']
    ],
    self::STATUS['MEMBRE_ACCUEILLI_RN'] => 
    [
      self::STATUS['MEMBRE_ACCUEILLI_RP'],
      self::STATUS['MEMBRE_ACCUEILLI_AA_P_AR'],
      self::STATUS['RADIE']
    ],
    self::STATUS['MEMBRE_ACCUEILLI_RP'] => 
    [
      self::STATUS['MEMBRE_ACCUEILLI_RN'],
      self::STATUS['MEMBRE_ACCUEILLI_AA_P_AR'],
      self::STATUS['RADIE']
    ],
    self::STATUS['MEMBRE_ACCUEILLI_AA_P_AR'] => 
    [      
      self::STATUS['MEMBRE_ACCUEILLI_RN'],
      self::STATUS['MEMBRE_ACCUEILLI_RP'],
      self::STATUS['RADIE']
    ],
    self::STATUS['SEMINARISTE_REGIME_CAVIMAC'] => 
    [      
      self::STATUS['SEMINARISTE_AUTRE_REGIME'],
      self::STATUS['MINISTRE_DU_CULTE'],
      self::STATUS['COTISANT_VOLONTAIRE'],
      self::STATUS['RADIE']
    ],
    self::STATUS['SEMINARISTE_AUTRE_REGIME'] => 
    [      
      self::STATUS['SEMINARISTE_REGIME_CAVIMAC'],
      self::STATUS['MINISTRE_DU_CULTE'],
      self::STATUS['COTISANT_VOLONTAIRE'],
      self::STATUS['RADIE']
    ],
  ];

  //Statuts disponible pour un membre de CATHO_ROM
  const CATHO_ROM_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 
      [        
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 
      [
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 
      [
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['POSTULANT_NOVICE_REGIME_CAVIMAC'] => 
      [
        self::STATUS['POSTULANT_NOVICE_AUTRE_REGIME'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['POSTULANT_NOVICE_AUTRE_REGIME'] => 
      [
        self::STATUS['POSTULANT_NOVICE_REGIME_CAVIMAC'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],



  ];

  //Statuts disponible pour un membre du culte ORTHODOX
  const ORTHO_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE']
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],

  ];

  //Statuts disponible pour un membre du culte ADVENTISTE
  const ADVENTISTE_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE']
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_RN'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_RP'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
      self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],

  ];

  //Statuts disponible pour un membre du culte ANGLICAN
  const ANGLICAN_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['RADIE']
      ],      
    self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
    self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] =>
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],    
  ];

  //Statuts disponible pour un membre du culte CATHOLIQUE_PRECHALCEDONIEN
  const CATHOLIQUE_PRECHALCEDONIEN_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['RADIE']
      ],      
    self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
    self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] =>
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],    
  ];

  //Statuts disponible pour un membre du culte MUSULMAN
  const MUSULMAN_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['RADIE']
      ],      
    self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
    self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] =>
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],    
  ];

  //Statuts disponible pour un membre du culte JUDAIQUE
  const JUDAIQUE_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['RADIE']
      ],      
    self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
    self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] =>
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],    
  ];

  //Statuts disponible pour un membre du culte AUTE CULTE D'ASIE
  const AUTRES_CULTES_DASIE_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],
        self::STATUS['RADIE']
      ],      
    self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],
    self::STATUS['MINISTRE_DU_CULTE_AA_P_AR'] =>
      [
        self::STATUS['MINISTRE_DU_CULTE_SANS_ACTIVITE'],        
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE']
      ],    
  ];

  //Statuts disponible pour un membre du culte CATHO_NON_ROM
  const CATHO_NON_ROM_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RN'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RP'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['RADIE'],
      ],


  ];

  //Statuts disponible pour un membre du culte CULTES_INSPIRATION_CHRETIENNE
  const CULTES_INSPIRATION_CHRETIENNE_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RN'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RP'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['RADIE'],
      ],


  ];

  //Statuts disponible pour un membre du culte TEMOINS_DE_JEHOVAH
  const TEMOINS_DE_JEHOVAH_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RN'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RP'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['RADIE'],
      ],
  ];

  //Statuts disponible pour un membre du culte HINDOUISTE
  const HINDOUISTE_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RN'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RP'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['RADIE'],
      ],
  ];

  //Statuts disponible pour un membre du culte BOUDDHISTE
  const BOUDDHISTE_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] =>
      [
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RN'] =>
      [        
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RP'] =>
      [        
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] =>
      [        
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
  ];

  //Statuts disponible pour un membre du culte PROTESTANT-EVANGELIQUE
  const PROT_EVAN_MODIFICATION = [
    self::STATUS['COTISANT_VOLONTAIRE'] => 
      [
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['COTISANT_VOLONTAIRE'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RN'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RP'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_RP'] => 
      [        
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
        self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
        self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
        self::STATUS['MEMBRE_RELIGIEUX_RN'],
        self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'],
        self::STATUS['RADIE'],
      ],
    self::STATUS['MEMBRE_RELIGIEUX_AA_P_AR'] => 
    [        
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRAITEMENT'],
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRAITEMENT'],
      self::STATUS['MINISTRE_DU_CULTE_AVEC_TRT_AA_P_AR'],
      self::STATUS['MINISTRE_DU_CULTE_SANS_TRT_AA_P_AR'],
      self::STATUS['MEMBRE_RELIGIEUX_RN'],
      self::STATUS['MEMBRE_RELIGIEUX_RP'],
      self::STATUS['RADIE'],
    ],
  ];
#Endregion

  /**
   * Renvoie une liste de cotisations pour un culte et un statut
   *
   * @param array $statusCotisationArray - Liste de toutes les cotisatations disponible pour un culte
   * @param int $calculatedStatusIndex - status index du membre
   * @return array - Liste des cotisations disponibles pour un statuts
   */
  function findCotisation($statusCotisationArray, $calculatedStatusIndex) {
      
    $cotisationAvail = [];
    foreach($statusCotisationArray as $item) {
      foreach ($item as $key => $value) {        
        if($key === $calculatedStatusIndex){        
          $cotisationAvail[] = $value;
        }
      }
    }
    
    return $cotisationAvail;
  }

  /**
   * Renvoie les CSG qui sont disponible pour un statut
   *
   * @param array $cotisation - Donnée de cotisations pour un statut
   * @return string - cotisation disponible format 1,2,3
   */
  function extractCsgAvailibility($cotisationsStatuts) {    
    $csgAvailibility = '';
    foreach($cotisationsStatuts as $cotisation) {
      $cotisation = explode(',', $cotisation);      
      if($cotisation[4] === 'true') {
        //CSG type 1
        $csgAvailibility .= '1,';
      } elseif ($cotisation[5] === 'true') {
        //CSG type 2
        $csgAvailibility .= '2,';
      } elseif ($cotisation[6] === 'true') {
        //CSG type 3
        $csgAvailibility .= '3,';
      } elseif ($cotisation[7] === 'true') {
        //CSG type 4
        $csgAvailibility .= '4,';
      } elseif ($cotisation[8] === 'true') {
        //CSG type 5
        $csgAvailibility .= '5,';
      } else {
        $csgAvailibility .= '-,';
      }
    }; 
    return substr_replace($csgAvailibility ,"",-1);;    
  }
}