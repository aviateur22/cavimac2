<?php

namespace Drupal\cavimac\Ajax;
use Drupal\Core\Ajax\CommandInterface;

class ImportFileCommand implements CommandInterface {

  protected $result; 

  public function __construct($result) {
    $this->result = $result;
    dump($this->result);
  }

  public function render()
  {
    return [
      'command'  => 'importFiles',
      'text' => $this->result
    ];
  }
}
